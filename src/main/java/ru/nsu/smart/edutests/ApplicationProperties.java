package ru.nsu.smart.edutests;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Data
@ConfigurationProperties(prefix = "application")
public class ApplicationProperties {
    private String domainName;

    private String path;

    private int port;
}
