package ru.nsu.smart.edutests;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.nsu.smart.edutests.exceptions.NotEnoughPointsException;
import ru.nsu.smart.edutests.exceptions.NotFoundException;

@ControllerAdvice
public class BaseExceptionHandler {
    @RequiredArgsConstructor
    @Getter
    public static final class ErrorModel {
        private final String message;
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ErrorModel> handleIllegalArgument(IllegalArgumentException exception) {
        return new ResponseEntity<>(new ErrorModel(exception.getLocalizedMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotEnoughPointsException.class)
    public ResponseEntity<ErrorModel> handleNotEnoughPoints(NotEnoughPointsException ex) {
        return new ResponseEntity<>(new ErrorModel(ex.getLocalizedMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorModel> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        var error = ex.getBindingResult().getFieldError();
        String errorString = error == null ? "" : error.getDefaultMessage();
        return new ResponseEntity<>(new ErrorModel(errorString), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorModel> handleNotFound(NotFoundException ex) {
        return new ResponseEntity<>(new ErrorModel(ex.getLocalizedMessage()), HttpStatus.NOT_FOUND);
    }
}
