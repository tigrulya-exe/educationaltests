package ru.nsu.smart.edutests.controllers;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.smart.edutests.dto.ProfileDto;
import ru.nsu.smart.edutests.dto.mappers.ProfileMapper;
import ru.nsu.smart.edutests.services.UsersService;
import ru.nsu.smart.edutests.util.AppPath;

@RestController
@RequiredArgsConstructor
@RequestMapping(AppPath.PATH_V1)
public class ProfileController {

    private final UsersService usersService;

    private final ProfileMapper profileMapper;

    @GetMapping("/profile")
    @ApiOperation("Получение профиля для текущего пользователя")
    public ProfileDto getProfile() {
        return profileMapper.toDto(usersService.getCurrentUser());
    }
}
