package ru.nsu.smart.edutests.controllers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.nsu.smart.edutests.dto.mappers.ProgressPartMapper;
import ru.nsu.smart.edutests.dto.models.ProgressPartDto;
import ru.nsu.smart.edutests.model.ProgressPart;
import ru.nsu.smart.edutests.services.ProgressPartService;

/**
 * Общий контроллер для всех этапов прогресса пользователя (Theme, Stage и т.д.)
 */
public class ProgressPartController<E extends ProgressPart> {
    @Getter
    private final ProgressPartService<E> service;

    @Autowired
    @Getter
    private ProgressPartMapper progressPartMapper;

    public ProgressPartController(ProgressPartService<E> service) { this.service = service; }

    @GetMapping("/{id}")
    @ApiOperation("Получение этапа прогресса в виде Data Transfer Object")
    public ProgressPartDto getEntity(
            @PathVariable
            @ApiParam("id этапа прогресса")
            int id) {
        return progressPartMapper.toDto(service.getEntity(id));
    }
}