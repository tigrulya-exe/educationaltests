package ru.nsu.smart.edutests.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.smart.edutests.model.Section;
import ru.nsu.smart.edutests.services.ProgressPartService;
import ru.nsu.smart.edutests.util.AppPath;

@RestController
@RequestMapping(AppPath.PATH_V1 + "/sections")
public class SectionsController extends ProgressPartController<Section> {
    public SectionsController(ProgressPartService<Section> service) { super(service); }
}
