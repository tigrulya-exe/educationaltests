package ru.nsu.smart.edutests.controllers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.nsu.smart.edutests.dto.StartTestResponseDto;
import ru.nsu.smart.edutests.dto.mappers.QuestionMapper;
import ru.nsu.smart.edutests.dto.mappers.StageMapper;
import ru.nsu.smart.edutests.dto.mappers.TestResultsMapper;
import ru.nsu.smart.edutests.dto.models.AnswerDto;
import ru.nsu.smart.edutests.dto.models.ProgressPartDto;
import ru.nsu.smart.edutests.dto.models.QuestionDto;
import ru.nsu.smart.edutests.dto.models.TestResultsDto;
import ru.nsu.smart.edutests.services.StagesService;
import ru.nsu.smart.edutests.services.TestService;
import ru.nsu.smart.edutests.util.AppPath;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(AppPath.PATH_V1 + "/stages")
public class StagesController {
    private final TestService testService;

    private final QuestionMapper questionMapper;

    private final TestResultsMapper testResultsMapper;

    private final StageMapper stageMapper;

    private final StagesService stagesService;

    @GetMapping("/{id}")
    @ApiOperation("Возвращает этап прогресса")
    public ProgressPartDto getEntity(
            @PathVariable
            @ApiParam("ID нужного этапа")
                int id) {

        return stageMapper.toDto(stagesService.getEntity(id));
    }

    @PostMapping("/test/{testStageId}/start")
    @ApiOperation("Начало теста")
    public StartTestResponseDto startTest(
            @PathVariable
            @ApiParam("ID теста, который собираемся начать")
            int testStageId) {

        int testProgressId = testService.startTest(testStageId).getId();
        return new StartTestResponseDto(testProgressId);
    }

    @GetMapping("/test/{testProgressId}/next")
    @ApiOperation("Получение следующего вопроса")
    public QuestionDto getNextQuestion(
            @PathVariable
            @ApiParam("ID сессии, для которой берем вопрос")
            int testProgressId) {

        return questionMapper.toDto(testService.getNextQuestion(testProgressId));
    }

    @PostMapping("/test/{testProgressId}/answer")
    @ApiOperation("Обработка ответа")
    public AnswerDto answerTheQuestion(
            @RequestBody
            @Valid
            @ApiParam("Ответ")
            AnswerDto answer,
            @PathVariable
            @ApiParam("ID сессии")
            int testProgressId) {

        var answerDto = new AnswerDto();
        answerDto.setId((testService.answerTheQuestion(answer.getId(), testProgressId)));
        return answerDto;
    }

    @GetMapping("/test/{testProgressId}/results")
    @ApiOperation("Получение результатов теста")
    public TestResultsDto getTestResults(
            @PathVariable
            @ApiParam("ID сессии")
            int testProgressId) {
        var testProgress = testService.getTestResults(testProgressId);
        return testResultsMapper.toDto(testProgress);
    }
}