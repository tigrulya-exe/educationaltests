package ru.nsu.smart.edutests.controllers;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.smart.edutests.dto.models.ProgressPartDto;
import ru.nsu.smart.edutests.model.Theme;
import ru.nsu.smart.edutests.services.ThemesService;
import ru.nsu.smart.edutests.util.AppPath;
import ru.nsu.smart.edutests.util.MapperUtils;

@RestController
@RequestMapping(AppPath.PATH_V1 + "/themes")
public class ThemesController extends ProgressPartController<Theme> {
    public ThemesController(ThemesService service) {
        super(service);
    }

    @GetMapping
    @ApiOperation("Получение всех тем")
    public Iterable<ProgressPartDto> getAllThemes() {
        return MapperUtils.entitiesToDtos(getService().getEntities(), getProgressPartMapper());
    }
}
