package ru.nsu.smart.edutests.controllers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.smart.edutests.dto.mappers.UnitMapper;
import ru.nsu.smart.edutests.dto.models.ProgressPartDto;
import ru.nsu.smart.edutests.model.Unit;
import ru.nsu.smart.edutests.services.UnitsService;
import ru.nsu.smart.edutests.util.AppPath;

@RestController
@RequestMapping(AppPath.PATH_V1 + "/units")
public class UnitsController extends ProgressPartController<Unit> {
    @Autowired
    private UnitMapper unitMapper;

    public UnitsController(
            @ApiParam("Сервис для работы с блоками вопросов")
            UnitsService service) {
        super(service);
    }

    @Override
    @GetMapping("/{id}")
    @ApiOperation("Получение блока вопросов в виде Data Transfer Object")
    public ProgressPartDto getEntity(
            @PathVariable
            @ApiParam("id блока вопросов")
            int id) {
        return unitMapper.toDto(getService().getEntity(id));
    }
}
