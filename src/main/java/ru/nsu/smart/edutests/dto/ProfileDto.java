package ru.nsu.smart.edutests.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProfileDto {

    private String name;

    private String email;

    private String fullName;

    private int totalPoints;

    private int levelNumber;

    private String levelName;
}
