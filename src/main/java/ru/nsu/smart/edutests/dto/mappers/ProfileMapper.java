package ru.nsu.smart.edutests.dto.mappers;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.nsu.smart.edutests.dto.ProfileDto;
import ru.nsu.smart.edutests.model.Level;
import ru.nsu.smart.edutests.model.User;

@Component
@RequiredArgsConstructor
public class ProfileMapper implements Mapper<User, ProfileDto> {

    private final ModelMapper modelMapper;

    @Override
    public ProfileDto toDto(User entity) {
        ProfileDto dto = modelMapper.map(entity, ProfileDto.class);
        Level level = entity.getLevel();
        dto.setLevelNumber(level.getId());
        dto.setLevelName(level.getName());
        return dto;
    }
}
