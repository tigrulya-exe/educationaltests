package ru.nsu.smart.edutests.dto.mappers;

import org.springframework.stereotype.Component;
import ru.nsu.smart.edutests.dto.models.InnerProgressPartDto;
import ru.nsu.smart.edutests.dto.models.ProgressPartDto;
import ru.nsu.smart.edutests.model.ProgressPart;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProgressPartMapper implements Mapper<ProgressPart, ProgressPartDto> {
    public ProgressPartDto toDto(ProgressPart entity) {
        ProgressPartDto dto = new ProgressPartDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setChildren(getInnerDtos(entity.getChildren()));
        return dto;
    }

    protected List<InnerProgressPartDto> getInnerDtos(List<? extends ProgressPart> children) {
        return children.stream()
                .map(c -> new InnerProgressPartDto(c.getId(), c.getName()))
                .collect(Collectors.toList());
    }
}
