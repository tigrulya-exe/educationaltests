package ru.nsu.smart.edutests.dto.mappers;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.nsu.smart.edutests.dto.models.AnswerDto;
import ru.nsu.smart.edutests.dto.models.QuestionDto;
import ru.nsu.smart.edutests.model.Question;

import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
// чтобы спринг смог заинджектить этот маппер в контроллер вопросов
@Component
public class QuestionMapper implements Mapper<Question, QuestionDto> {
    // маппер, который упрощает жизнь и маппит одинаковые поля (он может много чего)
    private final ModelMapper mapper;

    @Override
    public QuestionDto toDto(Question entity) {
        if (entity == null) {
            return null;
        }
        QuestionDto dto = mapper.map(entity, QuestionDto.class);
        Set<AnswerDto> answerDtos = entity.getWrongAnswers().stream()
                .map(a -> mapper.map(a, AnswerDto.class))
                .collect(Collectors.toSet());
        answerDtos.add(mapper.map(entity.getCorrectAnswer(), AnswerDto.class));
        dto.setAnswerDtos(answerDtos);
        return dto;
    }
}
