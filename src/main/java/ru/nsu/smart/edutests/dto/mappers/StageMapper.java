package ru.nsu.smart.edutests.dto.mappers;

import org.springframework.stereotype.Component;
import ru.nsu.smart.edutests.dto.models.StageDto;
import ru.nsu.smart.edutests.model.Stage;

@Component
public class StageMapper implements Mapper<Stage, StageDto> {
    @Override
    public StageDto toDto(Stage entity) {
        StageDto stageDto = new StageDto();
        stageDto.setText(entity.getText());
        stageDto.setId(entity.getId());
        stageDto.setName(entity.getName());

        return stageDto;
    }
}
