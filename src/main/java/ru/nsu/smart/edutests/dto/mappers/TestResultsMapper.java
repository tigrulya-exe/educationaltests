package ru.nsu.smart.edutests.dto.mappers;

import org.springframework.stereotype.Component;
import ru.nsu.smart.edutests.dto.models.TestResultsDto;
import ru.nsu.smart.edutests.model.UserTestProgress;

@Component
public class TestResultsMapper implements Mapper<UserTestProgress, TestResultsDto> {

    @Override
    public TestResultsDto toDto(UserTestProgress entity) {
        TestResultsDto testResultsDto = new TestResultsDto();
        testResultsDto.setCurrentPoints(entity.getPoints());
        testResultsDto.setMaxPoints(entity.getStage().getMaxPoints());
        return testResultsDto;
    }
}
