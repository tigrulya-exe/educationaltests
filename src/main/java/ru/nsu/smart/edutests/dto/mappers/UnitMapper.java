package ru.nsu.smart.edutests.dto.mappers;

import org.springframework.stereotype.Component;
import ru.nsu.smart.edutests.dto.models.InnerProgressPartDto;
import ru.nsu.smart.edutests.dto.models.InnerStageDto;
import ru.nsu.smart.edutests.model.ProgressPart;
import ru.nsu.smart.edutests.model.TestStage;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UnitMapper extends ProgressPartMapper {

    @Override
    protected List<InnerProgressPartDto> getInnerDtos(List<? extends ProgressPart> children) {
        return children.stream()
                .map(c -> new InnerStageDto(c.getId(), c.getName(), c instanceof TestStage))
                .collect(Collectors.toList());
    }
}
