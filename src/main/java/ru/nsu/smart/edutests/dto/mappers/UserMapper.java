package ru.nsu.smart.edutests.dto.mappers;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.nsu.smart.edutests.model.User;
import ru.nsu.smart.edutests.security.model.UserDto;

import javax.validation.Valid;

@Component
@RequiredArgsConstructor
public class UserMapper implements Mapper<User, UserDto> {
    private final ModelMapper modelMapper;

    @Override
    public User toEntity(@Valid UserDto dto) {
        return modelMapper.map(dto, User.class);
    }

    @Override
    public UserDto toDto(User entity) {
        return modelMapper.map(entity, UserDto.class);
    }
}
