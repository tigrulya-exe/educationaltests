package ru.nsu.smart.edutests.dto.models;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.PositiveOrZero;

// т.к. то что возвращается из контроллеров автоматом преобразуется в JSON
// то для корректной сериализации/ десериализации нужны геттеры и сеттеры на все поля + конструктор без аргументов
@Getter
@Setter
@NoArgsConstructor
public class AnswerDto {
    @PositiveOrZero
    @ApiModelProperty(value = "ID ответа", example = "1")
    private int id;

    @ApiModelProperty(value = "Текст ответа", example = "628 год до н. э.")
    private String answerText;
}
