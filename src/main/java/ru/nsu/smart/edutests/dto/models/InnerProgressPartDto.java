package ru.nsu.smart.edutests.dto.models;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Этот класс представляет дочернюю сущность для одной из частей иерархии прогресса юзера (Theme, Section, Unit)
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InnerProgressPartDto {
    @ApiModelProperty(value = "ID дочернего этапа", example = "1")
    private int id;

    @ApiModelProperty(value = "Название дочернего этапа", example = "Основание и царский период Рима")
    private String name;
}
