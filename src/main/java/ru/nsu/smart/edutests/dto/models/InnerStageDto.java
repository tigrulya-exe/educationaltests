package ru.nsu.smart.edutests.dto.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class InnerStageDto extends InnerProgressPartDto {
    public InnerStageDto(int id, String name, boolean isTest) {
        super(id, name);
        this.isTest = isTest;
    }

    private boolean isTest;
}