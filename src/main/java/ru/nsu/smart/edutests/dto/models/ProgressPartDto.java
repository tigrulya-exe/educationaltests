package ru.nsu.smart.edutests.dto.models;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


/**
 * Этот класс представляет из себя одну из частей иерархии прогресса юзера (Theme, Section, Unit)
 * и список дочерних ProgressPart для данной сущности.
 * <p>
 * Например: У Theme в children будет содержаться список всех дочерних Sections, у Section список Units и т.п.
 */
@Getter
@Setter
public class ProgressPartDto {
    @ApiModelProperty(value = "id этапа прогресса", example = "1")
    private int id;

    @ApiModelProperty(value = "Название этапа прогресса", example = "Античность")
    private String name;

    @ApiModelProperty(value = "Список дочерних этапов прогресса", example = "Древний Рим, Древняя Греция")
    private List<InnerProgressPartDto> children;
}
