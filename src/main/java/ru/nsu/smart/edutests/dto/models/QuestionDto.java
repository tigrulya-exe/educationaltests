package ru.nsu.smart.edutests.dto.models;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PositiveOrZero;
import java.util.Set;

@Getter
@Setter
// обычно из контроллеров не возвращают Entity, дабы скрыть внутреннюю реализацию бд
// и вместо них возвращают аналоги Entity - Data Transfer Object -
// если коротко, то это классы предназначенные для передачи основных объектов в виде, удобном для клиентов
// также внутри них делают валидацию полей (если сущность пришла от клиента) и навешивают аннотации для документации API
public class QuestionDto {
    @PositiveOrZero
    private int id;

    @NotEmpty
    private String questionText;

    @NotEmpty
    private Set<AnswerDto> answerDtos;
}
