package ru.nsu.smart.edutests.dto.models;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StageDto extends ProgressPartDto {
    @ApiModelProperty(value = "Теория для теоретической стадии и какой-либо текст для тестовой стадии")
    private String text;
}
