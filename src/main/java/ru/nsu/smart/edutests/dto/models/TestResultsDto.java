package ru.nsu.smart.edutests.dto.models;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

@Setter
@Getter
public class TestResultsDto {
    @PositiveOrZero
    @ApiModelProperty(value = "Количество набранных за тест очков", example = "1")
    private int currentPoints;

    @Positive
    @ApiModelProperty(value = "Максимальное количество очков за тест", example = "10")
    private int maxPoints;
}
