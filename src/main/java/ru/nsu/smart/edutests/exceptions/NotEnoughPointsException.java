package ru.nsu.smart.edutests.exceptions;

/**
 * Бросается в случае, если у пользователя недостаточно очков для получения любого этапа прогресса
 */
public class NotEnoughPointsException extends RuntimeException {
    public NotEnoughPointsException(String message) {
        super(message);
    }
}
