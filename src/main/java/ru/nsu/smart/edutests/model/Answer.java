package ru.nsu.smart.edutests.model;

import lombok.Getter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Entity
@Table(name = "Answers")
public class Answer extends Identifiable {

    private String answerText;
}
