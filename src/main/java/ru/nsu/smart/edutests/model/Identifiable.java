package ru.nsu.smart.edutests.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Родительский класс для всех энтити (у которых простые id)
 */
@MappedSuperclass
@Getter
@Setter
public class Identifiable {
    // у каждой Entity должен быть первичный ключ
    @Id
    // Говорим хибернейту, чтобы сам выбрал тип автоматической генерации primary key
    // аннотация в случае GenerationType.AUTO не нужна, т.к. это значение по дефолту, но добавил для примера
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

}
