package ru.nsu.smart.edutests.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Уровни пользователей
 */
@Getter
@Setter
@Entity
@Table(name = "Levels")
public class Level extends Identifiable {
    private int requiredPoints;

    /**
     * Что-то наподобие звания (новичок, любитель и тд)
     */
    private String name;
}
