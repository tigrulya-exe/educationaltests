package ru.nsu.smart.edutests.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.MappedSuperclass;
import java.util.List;

/**
 * Родительская сущность для всех этапов прогресса (Theme, Stage, Section, Unit)
 */
@Getter
@Setter
@MappedSuperclass
public abstract class ProgressPart extends Identifiable {
    private int requiredPoints;

    private String name;

    /**
     * Номер текущей ProgressPart в родительском ProgressPart
     */
    private int serialNumber;

    public abstract List<? extends ProgressPart> getChildren();
}
