package ru.nsu.smart.edutests.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "Questions")
public class Question extends Identifiable {

    private String questionText;

    @ManyToOne
    @JoinColumn(name = "correctAnswerId", referencedColumnName = "id")
    private Answer correctAnswer;

    @ManyToMany
    private Set<Answer> wrongAnswers;

    private int points;
}
