package ru.nsu.smart.edutests.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "Sections")
public class Section extends ProgressPart {
    @ManyToOne
    @JoinColumn(name = "themeId", referencedColumnName = "Id")
    private Theme theme;

    @OneToMany(mappedBy = "section")
    private List<Unit> units;

    @Override
    public List<Unit> getChildren() {
        return units;
    }
}
