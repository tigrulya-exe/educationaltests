package ru.nsu.smart.edutests.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "Stages")
@DiscriminatorValue("Theory")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Stage extends ProgressPart {
    @ManyToOne
    @JoinColumn(name = "unitId", referencedColumnName = "Id")
    private Unit unit;

    private int maxPoints;

    /**
     * Теория для теоретической стадии и какой-либо текст для тестовой стадии
     */
    @Column(columnDefinition = "text")
    private String text;

    @Override
    public List<? extends ProgressPart> getChildren() {
        return List.of();
    }
}
