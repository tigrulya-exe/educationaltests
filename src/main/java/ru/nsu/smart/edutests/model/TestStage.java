package ru.nsu.smart.edutests.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * Тестовая стадия с определенным количеством вопросов
 */
@Getter
@Setter
@Entity
@DiscriminatorValue("Test")
public class TestStage extends Stage {
    @ManyToMany
    @JoinTable(
            name = "TestsQuestions",
            joinColumns = @JoinColumn(name = "testId"),
            inverseJoinColumns = @JoinColumn(name = "questionId")
    )
    private List<Question> questions;
}
