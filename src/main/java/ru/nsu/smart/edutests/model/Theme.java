package ru.nsu.smart.edutests.model;

import lombok.Getter;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Getter
@Table(name = "Themes")
public class Theme extends ProgressPart {
    @OneToMany(mappedBy = "theme")
    private List<Section> sections;

    @Override
    public List<Section> getChildren() {
        return sections;
    }
}
