package ru.nsu.smart.edutests.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

@Getter
@Setter
@Entity
public class Unit extends ProgressPart {
    @ManyToOne
    @JoinColumn(name = "sectionId", referencedColumnName = "Id")
    private Section section;

    @OneToMany(mappedBy = "unit")
    private List<Stage> stages;

    @Override
    public List<Stage> getChildren() {
        return stages;
    }
}
