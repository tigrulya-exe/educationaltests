package ru.nsu.smart.edutests.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "Users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    private String name;

    private String email;

    private String fullName;

    /**
     * В базе хранится в захешированном через {@link org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder} виде.
     */
    private String password;

    //    @Formula("(select SUM(u.points) from UserStageProgress u where u.userId = id group by u.userId)")
    @Column
    private int totalPoints = 0;

    @OneToMany(mappedBy = "user")
    private Set<UserTestProgress> progressSet = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "UserRoles")
    private Set<UserRole> roles = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "levelId", referencedColumnName = "id")
    private Level level;
}
