package ru.nsu.smart.edutests.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "UserStageProgress")
public class UserTestProgress extends Identifiable {
    public enum Status {
        /**
         * Пользователь не ответил на текущий вопрос
         */
        IN_PROGRESS,
        /**
         * Пользователь ответил на текущий вопрос
         */
        ANSWERED,
        /**
         * Пользователь ответил на все вопросы теста
         */
        FINISHED
    }

    @Enumerated(EnumType.STRING)
    private Status status;

    @ManyToOne
    @JoinColumn(name = "userId", referencedColumnName = "id")
    private User user;

    @OneToOne
    @JoinColumn(name = "stageId", referencedColumnName = "id")
    private TestStage stage;

    private int currentQuestionIndex;

    private int points;

    public void incrementCurrentQuestionIndex() {
        ++currentQuestionIndex;
    }
}
