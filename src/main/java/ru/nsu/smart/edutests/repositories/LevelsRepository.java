package ru.nsu.smart.edutests.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.smart.edutests.model.Level;

public interface LevelsRepository extends JpaRepository<Level, Integer> {
}
