package ru.nsu.smart.edutests.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.smart.edutests.model.Question;

//@Repository внутри это тот же @Component, просто явно указываем, что данный класс у нас является
// DAO - Data Access Object - по сути это интерфейс Spring Data Jpa для удобного взаимодействия с бд
@Repository
// С помощью одной строки мы получаем все методы для CRUD объекта Question!
public interface QuestionsRepository extends CrudRepository<Question, Integer> {
    // возможно этот метод нам не понадобится, но!
    // магия спринга в том, что нам не надо реализовывать этот метод, спринг сделает это за нас!
    // то есть мы можем писать множество комбинаций, напимер
    // getDistinctFirstByCorrectAnswerIdAndQuestionText(int correctAnswerId, String questionText)
    // и спринг сгенерирует метод, который будет возвращать объект Question по айдишнику правильного ответа
    // и тексту вопроса!
    // а этот пример ищет вопрос по тексту вопроса
    Question findByQuestionText(String questionText);
}
