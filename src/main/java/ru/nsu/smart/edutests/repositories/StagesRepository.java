package ru.nsu.smart.edutests.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.smart.edutests.model.Stage;

@Repository
public interface StagesRepository extends JpaRepository<Stage, Integer> {
}
