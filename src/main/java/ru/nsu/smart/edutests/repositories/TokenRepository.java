package ru.nsu.smart.edutests.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.smart.edutests.model.User;
import ru.nsu.smart.edutests.security.model.Token;

import java.util.Optional;

@Repository
public interface TokenRepository extends JpaRepository<Token, Integer> {
    void deleteByUserAndType(User user, Token.Type type);

    Optional<Token> findByStringRepresentationAndType(String stringRepresentation, Token.Type type);
}
