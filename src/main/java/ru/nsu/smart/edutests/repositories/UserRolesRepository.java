package ru.nsu.smart.edutests.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.smart.edutests.model.UserRole;

@Repository
public interface UserRolesRepository extends JpaRepository<UserRole, Integer> {
    UserRole findByRole(UserRole.Role role);
}
