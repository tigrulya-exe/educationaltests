package ru.nsu.smart.edutests.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.smart.edutests.model.Stage;
import ru.nsu.smart.edutests.model.User;
import ru.nsu.smart.edutests.model.UserTestProgress;

import java.util.List;

@Repository
public interface UserTestProgressRepository extends JpaRepository<UserTestProgress, Integer> {
    List<UserTestProgress> findByUserAndStage(User user, Stage stage);
}
