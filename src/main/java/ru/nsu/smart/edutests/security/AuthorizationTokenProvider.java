package ru.nsu.smart.edutests.security;

import java.util.Date;

public interface AuthorizationTokenProvider {
    String generateToken(int userId);

    boolean validateToken(String token);

    Date getExpirationDate(String token);

    String getUserId(String token);
}