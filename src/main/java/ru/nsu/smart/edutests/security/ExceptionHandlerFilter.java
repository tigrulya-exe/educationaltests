package ru.nsu.smart.edutests.security;

import io.jsonwebtoken.JwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import ru.nsu.smart.edutests.exceptions.NotFoundException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Обработчик исключений, бросаемых из методов по авторизации
 */
@Component
@Slf4j
public class ExceptionHandlerFilter extends OncePerRequestFilter {
    @Override
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            filterChain.doFilter(request, response);
        } catch (JwtException | IllegalArgumentException | ArrayIndexOutOfBoundsException | NotFoundException e) {
            // тут же у сообщения будет уровень ERROR
            log.error("Authorization error during http filtering", e);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        } catch (RuntimeException e) {
            log.error("Error during http filtering", e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
