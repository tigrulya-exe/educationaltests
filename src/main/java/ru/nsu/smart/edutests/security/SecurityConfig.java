package ru.nsu.smart.edutests.security;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import ru.nsu.smart.edutests.ApplicationProperties;
import ru.nsu.smart.edutests.model.UserRole.Role;
import ru.nsu.smart.edutests.security.jwt.JwtAuthenticationProvider;
import ru.nsu.smart.edutests.security.jwt.JwtRequestFilter;

/**
 * Общая конфигурация Spring Security
 */
@Configuration
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final JwtAuthenticationProvider jwtAuthenticationProvider;

    private final JwtRequestFilter jwtRequestFilter;

    private final ApplicationProperties properties;

    private final ExceptionHandlerFilter exceptionHandlerFilter;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(jwtAuthenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.httpBasic().disable();
        http.addFilterBefore(jwtRequestFilter, AbstractPreAuthenticatedProcessingFilter.class);
        http.addFilterBefore(exceptionHandlerFilter, JwtRequestFilter.class);
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.anonymous()
                .and().authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .antMatchers("/test**").hasAuthority(Role.DEFAULT.name())
                .antMatchers(properties.getPath() + "/units/**").hasAuthority(Role.DEFAULT.name())
                .antMatchers(properties.getPath() + "/sections/**").hasAuthority(Role.DEFAULT.name())
                .antMatchers(properties.getPath() + "/stages/**").hasAuthority(Role.DEFAULT.name())
                .antMatchers(properties.getPath() + "/themes/**").hasAuthority(Role.DEFAULT.name())
                .antMatchers(properties.getPath() + "/profile/**").hasAuthority(Role.DEFAULT.name());
    }
}
