package ru.nsu.smart.edutests.security.controllers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.nsu.smart.edutests.dto.mappers.UserMapper;
import ru.nsu.smart.edutests.security.model.Credentials;
import ru.nsu.smart.edutests.security.model.NewPasswordDto;
import ru.nsu.smart.edutests.security.model.TokenDto;
import ru.nsu.smart.edutests.security.model.UserDto;
import ru.nsu.smart.edutests.security.services.AuthorizationService;

import javax.validation.Valid;

@RestController
@RequestMapping
@RequiredArgsConstructor
@Slf4j
public class AuthorizationController {
    private final AuthorizationService authorizationService;

    private final UserMapper userMapper;

    @PostMapping("/sign-in")
    @ApiOperation("Вход в систему")
    public TokenDto signIn(
            @RequestBody
            @Valid
            @ApiParam("Структура с логином и паролем пользователя")
                    Credentials credentials) {

        return authorizationService.authenticate(credentials);
    }

    @PostMapping("/sign-up")
    @ApiOperation("Регистрация")
    public void signUp(
            @RequestBody
            @Valid
            @ApiParam("Данные пользователя для регистрации")
                    UserDto userAuthorizationDto) {

        authorizationService.signUp(userMapper.toEntity(userAuthorizationDto));
    }

    @PostMapping("/restore")
    @ApiOperation("Запрос на восстановление пароля")
    public void restore(
            @RequestParam
            @ApiParam("Почта, на которую будет отправлена форма для восстановления")
                    String email) {

        authorizationService.restorePassword(email);
    }

    @PostMapping("/restore/{token}")
    @ApiOperation("Смена пароля при восстановлении пароля")
    public void restore(
            @RequestBody
            @Valid NewPasswordDto newPasswordDto,
            @PathVariable String token) {

        String newPassword = newPasswordDto.getNewPassword();
        authorizationService.changePassword(token, newPassword);
    }

    @GetMapping("/restore/{token}")
    @ApiOperation("Получение формы для восстановления пароля")
    public ModelAndView getRestorePage(@PathVariable String token) {
        authorizationService.validateRestoreToken(token);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/restore.html");

        log.debug("The restore page has been queried.");
        return modelAndView;
    }

    @GetMapping("/confirm")
    @ApiOperation("Подтверждение почты")
    public ModelAndView confirmRegistration(@RequestParam String token) {
        authorizationService.confirmEmail(token);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/emailConfirmSuccess.html");

        return modelAndView;
    }
}
