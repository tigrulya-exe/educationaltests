package ru.nsu.smart.edutests.security.jwt;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import ru.nsu.smart.edutests.model.User;

import java.util.Collection;

/**
 * Этот объект просто представляет из себя авторизация с помощью Json Web Token
 */

public class JwtAuthentication implements Authentication {
    @Setter
    private transient User user;

    private boolean isAuthenticated;

    @Getter
    private final String jwt;

    public JwtAuthentication(String jwt) {
        this.jwt = jwt;
        this.isAuthenticated = false;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return user.getRoles();
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getDetails() {
        return null;
    }


    /**
     * @return текущего пользователя
     */
    @Override
    public Object getPrincipal() {
        return user;
    }

    @Override
    public boolean isAuthenticated() {
        return isAuthenticated;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) {
        this.isAuthenticated = isAuthenticated;
    }

    @Override
    public String getName() {
        return user.getName();
    }
}
