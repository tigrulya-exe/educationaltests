package ru.nsu.smart.edutests.security.listeners;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import ru.nsu.smart.edutests.ApplicationProperties;
import ru.nsu.smart.edutests.email.EmailService;
import ru.nsu.smart.edutests.model.User;
import ru.nsu.smart.edutests.security.events.PasswordRestoreEvent;
import ru.nsu.smart.edutests.security.model.Token;
import ru.nsu.smart.edutests.security.services.TokenService;

@Component
@RequiredArgsConstructor
@Slf4j
public class PasswordRestoreListener implements ApplicationListener<PasswordRestoreEvent> {

    private final ApplicationProperties applicationProperties;

    private final TokenService tokenService;

    private final EmailService emailService;

    /**
     * Этот метод вызывается, когда случился {@link PasswordRestoreEvent}
     * Отправляет на почту пользователя сообщение с ссылкой для восстановления пароля.
     * Тут генерируется {@link Token} восстановления пароля
     */

    @Override
    public void onApplicationEvent(PasswordRestoreEvent event) {
        User user = event.getUser();
        String domainName = applicationProperties.getDomainName();
        int port = applicationProperties.getPort();

        try {
            Token token = tokenService.generateToken(user, Token.Type.PASSWORD_RESTORE);
            String confirmUrl = "http://" + domainName + ":" + port + "/restore/" + token.getStringRepresentation();
            String message = "Здравствуйте, " + user.getName()
                    + "!\nНажмите на ссылку ниже, чтобы поменять пароль:\n" + confirmUrl;
            emailService.sendMessage(user.getEmail(), "Смена пароля", message);
        } catch (Exception exc) {
            log.error("Error sending password restore link to user: {}", user.getEmail(), exc);
        }
    }
}
