package ru.nsu.smart.edutests.security.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Pattern;

import static ru.nsu.smart.edutests.util.RegexTemplateConstants.*;

@Data
@NoArgsConstructor
public class Credentials {
    // больше 3 символов, буквы латинского алфавита, тире, нижнее подчеркивание, точка либо валидный емейл
    @Pattern(regexp = LOGIN + "|" + EMAIL, message = "Поле 'Логин' некорректно")
    @ApiModelProperty(value = "Логин пользователя (номер телефона либо почта)", example = "ivan_ivanich1337@gmail.com")
    private String login;

    // минимум 8 символов - латинские буквы, минимум 1 заглавная, минимум 1 цифра
    @Pattern(regexp = PASSWORD, message = "Поле 'Пароль' некорректно")
    @ApiModelProperty(value = "Пароль", example = "SuperSecret123")
    private String password;
}