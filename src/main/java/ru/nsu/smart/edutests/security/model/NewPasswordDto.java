package ru.nsu.smart.edutests.security.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Pattern;

import static ru.nsu.smart.edutests.util.RegexTemplateConstants.PASSWORD;

@Data
public class NewPasswordDto {
    @Pattern(regexp = PASSWORD, message = "Некорректное поле 'Новый пароль'")
    @ApiModelProperty(value = "Новый пароль", required = true, example = "SuperSecret123")
    private String newPassword;
}
