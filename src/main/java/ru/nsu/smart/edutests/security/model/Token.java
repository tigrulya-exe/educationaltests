package ru.nsu.smart.edutests.security.model;

import lombok.Getter;
import lombok.Setter;
import ru.nsu.smart.edutests.model.Identifiable;
import ru.nsu.smart.edutests.model.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "Tokens")
public class Token extends Identifiable {
    public enum Type {
        REFRESH,
        PASSWORD_RESTORE,
        EMAIL_CONFIRM
    }

    @NotNull
    String stringRepresentation;

    @NotNull
    @OneToOne
    @JoinColumn(name = "userId", referencedColumnName = "id")
    User user;

    @Temporal(TemporalType.TIMESTAMP)
    Date expirationDate;

    @Enumerated(EnumType.STRING)
    Type type;
}