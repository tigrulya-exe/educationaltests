package ru.nsu.smart.edutests.security.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
public class TokenDto {
    @ApiModelProperty(value = "Строковое представление токена", required = true)
    @NotBlank
    private String token;
}