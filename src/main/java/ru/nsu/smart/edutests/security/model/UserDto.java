package ru.nsu.smart.edutests.security.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static ru.nsu.smart.edutests.util.RegexTemplateConstants.*;

@Data
public class UserDto {
    @Size(min = 2, max = 20)
    @ApiModelProperty(value = "Имя", required = true, example = "Vanya227")
    String name;

    @Pattern(regexp = EMAIL)
    @ApiModelProperty(value = "Электронная почта", required = true, example = "ivan_ivanich1337@gmail.com")
    String email;

    @Size(min = 3, max = 50)
    @ApiModelProperty(value = "Полное имя", required = true, example = "Челепышев Геннадий Васильевич")
    String fullName;

    @Pattern(regexp = PASSWORD)
    @ApiModelProperty(value = "Пароль", required = true, example = "SuperSecret123")
    String password;
}