package ru.nsu.smart.edutests.security.services;

import ru.nsu.smart.edutests.model.User;
import ru.nsu.smart.edutests.security.model.Credentials;
import ru.nsu.smart.edutests.security.model.TokenDto;

import javax.validation.Valid;

public interface AuthorizationService {
    void signUp(User user);

    TokenDto authenticate(@Valid Credentials credentials);

    void restorePassword(String email);

    void validateRestoreToken(String token);

    void changePassword(String stringToken, String newPassword);

    void confirmEmail(String stringToken);
}
