package ru.nsu.smart.edutests.security.services;

import ru.nsu.smart.edutests.model.User;
import ru.nsu.smart.edutests.security.model.Token;

import javax.validation.Valid;

public interface TokenService {

    User getUser(String stringToken, Token.Type type);

    boolean isTokenExpired(@Valid Token token);

    Token validateToken(String stringToken, Token.Type type);

    void removeToken(int id);

    Token generateToken(User user, Token.Type type);
}
