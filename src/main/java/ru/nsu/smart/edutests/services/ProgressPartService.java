package ru.nsu.smart.edutests.services;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.smart.edutests.exceptions.NotEnoughPointsException;
import ru.nsu.smart.edutests.exceptions.NotFoundException;
import ru.nsu.smart.edutests.model.ProgressPart;
import ru.nsu.smart.edutests.model.User;

import java.util.List;

/**
 * Родительский сервис для всех этапов прогресса
 */
@RequiredArgsConstructor
public class ProgressPartService<E extends ProgressPart> {
    private UsersService usersService;

    @Getter
    private final JpaRepository<E, Integer> repository;

    @Autowired
    public void setAuthorizationService(UsersService usersService) {
        this.usersService = usersService;
    }

    /**
     * Получение этапа прогресса под номером id
     */
    public E getEntity(int id) {
        E progressPart = repository
                .findById(id)
                .orElseThrow(() -> new NotFoundException("Wrong id"));
        checkRequiredPoints(usersService.getCurrentUser(), progressPart);
        return progressPart;
    }

    public List<E> getEntities() {
        return repository.findAll();
    }

    /**
     * Проверяем, хватает ли у пользователя очков для получения данного этапа
     *
     * @throws NotEnoughPointsException Если очков не хватает
     */
    private void checkRequiredPoints(User user, E progressPart) {
        if (user.getTotalPoints() < progressPart.getRequiredPoints()) {
            throw new NotEnoughPointsException("Not enough points");
        }
    }
}
