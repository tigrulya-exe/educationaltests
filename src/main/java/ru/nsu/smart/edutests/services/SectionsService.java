package ru.nsu.smart.edutests.services;

import org.springframework.stereotype.Service;
import ru.nsu.smart.edutests.model.Section;
import ru.nsu.smart.edutests.repositories.SectionsRepository;

@Service
public class SectionsService extends ProgressPartService<Section> {
    public SectionsService(SectionsRepository repository) {
        super(repository);
    }
}
