package ru.nsu.smart.edutests.services;

import org.springframework.stereotype.Service;
import ru.nsu.smart.edutests.model.Stage;
import ru.nsu.smart.edutests.repositories.StagesRepository;

@Service
public class StagesService extends ProgressPartService<Stage> {
    public StagesService(StagesRepository repository) {
        super(repository);
    }
}
