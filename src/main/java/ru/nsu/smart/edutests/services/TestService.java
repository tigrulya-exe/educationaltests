package ru.nsu.smart.edutests.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.smart.edutests.exceptions.NotFoundException;
import ru.nsu.smart.edutests.model.Question;
import ru.nsu.smart.edutests.model.TestStage;
import ru.nsu.smart.edutests.model.User;
import ru.nsu.smart.edutests.model.UserTestProgress;
import ru.nsu.smart.edutests.repositories.TestsRepository;
import ru.nsu.smart.edutests.repositories.UserTestProgressRepository;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * Сервис, отвечающий за всю логику с тестами
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class TestService {
    private final UsersService usersService;

    private final TestsRepository testsRepository;

    private final UserTestProgressRepository userTestProgressRepository;

    /**
     * Метод, отвечающий за генерацию нового {@link UserTestProgress}.
     * Такое событие происходит, когда пользователь решает начать тест с айдишником testStageId
     *
     * @param testStageId Id теста, который пользователь желает пройти
     * @return Айдишник сгенерированного объекта {@link UserTestProgress}
     */
    @Transactional
    public UserTestProgress startTest(int testStageId) {
        User user = usersService.getCurrentUser();
        TestStage testStage = getTestById(testStageId);
        UserTestProgress userTestProgress = new UserTestProgress();

        log.info("User {} starts test {}", user.getName(), testStageId);

        userTestProgress.setPoints(0);
        userTestProgress.setStatus(UserTestProgress.Status.ANSWERED);
        userTestProgress.setCurrentQuestionIndex(0);
        userTestProgress.setStage(testStage);
        userTestProgress.setUser(user);

        return userTestProgressRepository.save(userTestProgress);
    }

    /**
     * Получение следующего вопроса в тесте из сессии ({@link UserTestProgress}).
     * То есть фронт заранее не знает какой вопрос идет далее. Это известно только на сервере.
     * Это добавлено для безопасности
     * (чтобы юзер запросами не нагенерил себе миллионы пойнтов, отправляя нерпавильные айди вопросов)
     * и возможности расширения (бесконечный режим; режим, где вопросы достаются из бд динамически в рандомном порядке и т.п.)
     * Если пользователь не ответил на текущий вопрос, то сервер будет выдавать только его, пока клиент на него не ответит.
     *
     * @param userTestProgressId Id сессии.
     * @return Следующий вопрос, если такой есть, иначе null.
     */
    @Transactional
    public Question getNextQuestion(int userTestProgressId) {
        UserTestProgress userTestProgress = getUserTestProgressById(userTestProgressId);
        usersService.validateUser(userTestProgress.getUser());
        Question question = null;
        List<Question> questions = userTestProgress.getStage().getQuestions();

        try {
            int currentQuestionIndex = userTestProgress.getCurrentQuestionIndex();
            question = questions.get(currentQuestionIndex);
            userTestProgress.setStatus(UserTestProgress.Status.IN_PROGRESS);
            log.info("User {} queried question {} from test {}", userTestProgress.getUser().getName(),
                    currentQuestionIndex, userTestProgress.getStage().getId());

        } catch (IndexOutOfBoundsException exc) {
            finishTest(userTestProgress);
            log.info("User {} finished test", userTestProgress.getUser().getName());
        }

        return question;
    }

    /**
     * Метод, обрабатывающий ответ пользователя на вопрос из теста.
     * Фронт не знает заранее правильного ответа.
     *
     * @param answerId       id ответа пользователя.
     * @param testProgressId id текущей сесии теста ({@link UserTestProgress}).
     * @return id правильного ответа на вопрос.
     */
    @Transactional
    public int answerTheQuestion(int answerId, int testProgressId) {
        try {
            UserTestProgress userTestProgress = getUserTestProgressById(testProgressId);
            usersService.validateUser(userTestProgress.getUser());
            int currentQuestionIndex = userTestProgress.getCurrentQuestionIndex();
            Question currentQuestion = userTestProgress.getStage().getQuestions().get(currentQuestionIndex);

            // юзер ответил правильно
            if (currentQuestion.getCorrectAnswer().getId() == answerId) {
                addPoints(userTestProgress, currentQuestion.getPoints());
            }
            userTestProgress.setStatus(UserTestProgress.Status.ANSWERED);
            userTestProgress.incrementCurrentQuestionIndex();
            return currentQuestion.getCorrectAnswer().getId();
        } catch (IndexOutOfBoundsException exc) {
            throw new IllegalArgumentException("Wrong test progress id");
        }
    }


    /**
     * Получить результаты оконченного теста
     *
     * @param testProgressId Id сессии, результаты теста которого нужно узнать
     * @throws IllegalArgumentException Если тест еще не окончен
     */
    public UserTestProgress getTestResults(int testProgressId) {
        UserTestProgress userTestProgress = getUserTestProgressById(testProgressId);
        if (userTestProgress.getStatus() != UserTestProgress.Status.FINISHED) {
            throw new IllegalArgumentException("Test is not finished yet");
        }
        log.info("User {} finished test {}, sessionId {}", userTestProgress.getUser().getName(),
                userTestProgress.getStage().getId(), testProgressId);
        return userTestProgress;
    }

    /**
     * Добавление пойнтов за тест пользователю.
     * Если до этого у пользователя были завершенные попытки пройти этот тест, то баллы за текущую
     * попытку добавляются только если в текущей попытке набрано наибольшее количество баллов из всех попыток.
     */
    @Transactional
    public void finishTest(UserTestProgress userTestProgress) {
        User user = userTestProgress.getUser();

        int maxPreviousTestPoints = getMaxPreviousTestPoints(userTestProgress);
        int currentTestPoints = userTestProgress.getPoints();

        if (currentTestPoints > maxPreviousTestPoints || maxPreviousTestPoints == 0) {
            user.setTotalPoints(user.getTotalPoints() + currentTestPoints - maxPreviousTestPoints);
        }

        userTestProgress.setStatus(UserTestProgress.Status.FINISHED);

        log.info("User {} gain {} points for test {}", userTestProgress.getUser().getName(),
                currentTestPoints, userTestProgress.getStage().getId());
    }

    /**
     * Обновление очков текущей сесии, если пользователь ответил правильно.
     */
    private void addPoints(UserTestProgress userTestProgress, int newPoints) {
        int currentPoints = userTestProgress.getPoints();
        userTestProgress.setPoints(currentPoints + newPoints);
    }

    /**
     * Находит максимальное количесто очков за данный тест, полученных за предыдущие попытки.
     *
     * @return Максимальное количество очков, если такое есть, 0 иначе.
     */
    private int getMaxPreviousTestPoints(UserTestProgress userTestProgress) {
        User user = userTestProgress.getUser();
        TestStage testStage = userTestProgress.getStage();

        List<UserTestProgress> progresses = userTestProgressRepository.findByUserAndStage(user, testStage);
        Optional<UserTestProgress> maxPointsTestProgress = progresses.stream()
                .filter(u -> u.getId() != userTestProgress.getId())
                .max(Comparator.comparingInt(UserTestProgress::getPoints));

        log.info("User {} finished test {}", userTestProgress.getUser().getName(),
                userTestProgress.getStage().getId());
        return maxPointsTestProgress.map(UserTestProgress::getPoints).orElse(0);
    }

    private TestStage getTestById(int id) {
        return testsRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException("Wrong id"));
    }

    private UserTestProgress getUserTestProgressById(int id) {
        return userTestProgressRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException("Wrong id"));
    }
}
