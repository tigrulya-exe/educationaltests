package ru.nsu.smart.edutests.services;

import org.springframework.stereotype.Service;
import ru.nsu.smart.edutests.model.Theme;
import ru.nsu.smart.edutests.repositories.ThemesRepository;

@Service
public class ThemesService extends ProgressPartService<Theme> {
    public ThemesService(ThemesRepository repository) {
        super(repository);
    }
}
