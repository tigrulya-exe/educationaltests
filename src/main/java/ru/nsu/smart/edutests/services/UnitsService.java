package ru.nsu.smart.edutests.services;

import org.springframework.stereotype.Service;
import ru.nsu.smart.edutests.model.Unit;
import ru.nsu.smart.edutests.repositories.UnitsRepository;

@Service
public class UnitsService extends ProgressPartService<Unit> {

    public UnitsService(UnitsRepository repository) {
        super(repository);
    }
}
