package ru.nsu.smart.edutests.util;

public class AppPath {
    private AppPath() {
    }

    public static final String PATH_V1 = "/api/v1";
}
