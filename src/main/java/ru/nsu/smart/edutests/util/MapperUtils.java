package ru.nsu.smart.edutests.util;

import ru.nsu.smart.edutests.dto.mappers.Mapper;

import java.util.List;
import java.util.stream.Collectors;

public class MapperUtils {
    private MapperUtils() {
    }

    public static <E, D> List<D> entitiesToDtos(List<E> entities, Mapper<? super E, D> mapper) {
        return entities.stream()
                .map(mapper::toDto)
                .collect(Collectors.toList());
    }
}
