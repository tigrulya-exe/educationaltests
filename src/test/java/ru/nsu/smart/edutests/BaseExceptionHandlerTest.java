package ru.nsu.smart.edutests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import ru.nsu.smart.edutests.exceptions.NotEnoughPointsException;
import ru.nsu.smart.edutests.exceptions.NotFoundException;

public class BaseExceptionHandlerTest {

    private static final String TEST_MESSAGE = "test";

    @Mock
    private MethodArgumentNotValidException methodArgumentNotValidException;

    @Mock
    private FieldError fieldError;

    @Mock
    private BindingResult bindingResult;

    private BaseExceptionHandler baseExceptionHandler = new BaseExceptionHandler();

    @Test
    @DisplayName("Тест для IllegalArgumentException")
    public void handleIllegalArgumentTest() {
        IllegalArgumentException exception = new IllegalArgumentException();
        ResponseEntity<BaseExceptionHandler.ErrorModel> responseEntity = baseExceptionHandler
                .handleIllegalArgument(exception);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        Assertions.assertEquals(responseEntity.getBody().getMessage(), exception.getLocalizedMessage());
    }

    @Test
    @DisplayName("Тест для NotEnoughPointsException")
    public void handleNotEnoughPointsTest() {
        NotEnoughPointsException exception = new NotEnoughPointsException(TEST_MESSAGE);
        ResponseEntity<BaseExceptionHandler.ErrorModel> responseEntity = baseExceptionHandler
                .handleNotEnoughPoints(exception);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        Assertions.assertEquals(responseEntity.getBody().getMessage(), exception.getLocalizedMessage());
    }

    @Test
    @DisplayName("Тест для MethodArgumentNotValid с not null error")
    public void handleMethodArgumentNotValid() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(methodArgumentNotValidException.getBindingResult()).thenReturn(bindingResult);
        Mockito.when(bindingResult.getFieldError()).thenReturn(fieldError);
        Mockito.when(fieldError.getDefaultMessage()).thenReturn(TEST_MESSAGE);
        ResponseEntity<BaseExceptionHandler.ErrorModel> responseEntity = baseExceptionHandler
                .handleMethodArgumentNotValid(methodArgumentNotValidException);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        Assertions.assertEquals(TEST_MESSAGE, responseEntity.getBody().getMessage());
    }

    @Test
    @DisplayName("Тест для MethodArgumentNotValid с null error")
    public void handleMethodArgumentNotValidNull() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(methodArgumentNotValidException.getBindingResult()).thenReturn(bindingResult);
        Mockito.when(bindingResult.getFieldError()).thenReturn(null);
        ResponseEntity<BaseExceptionHandler.ErrorModel> responseEntity = baseExceptionHandler
                .handleMethodArgumentNotValid(methodArgumentNotValidException);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        Assertions.assertEquals("", responseEntity.getBody().getMessage());
    }

    @Test
    @DisplayName("Тест для NotFound")
    public void handleNotFoundTest() {
        NotFoundException notFoundException = new NotFoundException(TEST_MESSAGE);
        ResponseEntity<BaseExceptionHandler.ErrorModel> responseEntity = baseExceptionHandler
                .handleNotFound(notFoundException);
        Assertions.assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        Assertions.assertEquals(TEST_MESSAGE, responseEntity.getBody().getMessage());

    }
}
