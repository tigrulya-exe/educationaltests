package ru.nsu.smart.edutests.controllers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.nsu.smart.edutests.dto.ProfileDto;
import ru.nsu.smart.edutests.dto.mappers.ProfileMapper;
import ru.nsu.smart.edutests.model.User;
import ru.nsu.smart.edutests.services.UsersService;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ImportAutoConfiguration(exclude = {FlywayAutoConfiguration.class})
@TestPropertySource(locations = "classpath:application-test.properties")
public class ProfileControllerTest {

    @Autowired
    private ProfileController profileController;

    @MockBean
    private UsersService usersService;

    @MockBean
    private ProfileMapper profileMapper;

    @Test
    public void getProfile() {
        User user = new User();
        user.setName("Vasya");
        ProfileDto profileDto = new ProfileDto();
        profileDto.setName(user.getName());

        when(usersService.getCurrentUser()).thenReturn(user);
        when(profileMapper.toDto(user)).thenReturn(profileDto);

        ProfileDto returnedProfile = profileController.getProfile();
        assertThat(returnedProfile, is(profileDto));
    }
}
