package ru.nsu.smart.edutests.controllers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.nsu.smart.edutests.dto.StartTestResponseDto;
import ru.nsu.smart.edutests.dto.mappers.QuestionMapper;
import ru.nsu.smart.edutests.dto.mappers.StageMapper;
import ru.nsu.smart.edutests.dto.mappers.TestResultsMapper;
import ru.nsu.smart.edutests.dto.models.AnswerDto;
import ru.nsu.smart.edutests.dto.models.QuestionDto;
import ru.nsu.smart.edutests.dto.models.TestResultsDto;
import ru.nsu.smart.edutests.model.Answer;
import ru.nsu.smart.edutests.model.Question;
import ru.nsu.smart.edutests.model.TestStage;
import ru.nsu.smart.edutests.model.UserTestProgress;
import ru.nsu.smart.edutests.repositories.UserTestProgressRepository;
import ru.nsu.smart.edutests.services.StagesService;
import ru.nsu.smart.edutests.services.TestService;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;
import static org.hamcrest.CoreMatchers.is;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ImportAutoConfiguration(exclude = {FlywayAutoConfiguration.class})
@TestPropertySource(locations = "classpath:application-test.properties")
public class StageControllerTest {


    @MockBean
    private UserTestProgressRepository userTestProgressRepository;

    @MockBean
    private TestService testService;

    @MockBean
    private QuestionMapper questionMapper;

    @MockBean
    private TestResultsMapper testResultsMapper;

    @MockBean
    private StageMapper stageMapper;

    @MockBean
    private StagesService stagesService;

    @Autowired
    private StagesController stagesController;

    @Test
    public void startTestTest (){
        TestStage testStage = new TestStage();
        testStage.setQuestions(List.of(new Question()));
        UserTestProgress userTestProgress = generateUserTestProgress(testStage, 0);

        StartTestResponseDto startTestResponseDto = new StartTestResponseDto();
        startTestResponseDto.setUserTestStageProgressId(userTestProgress.getId());

        when(testService.startTest(userTestProgress.getId())).thenReturn(userTestProgress);

        StartTestResponseDto returnedResponseDto = stagesController.startTest(userTestProgress.getId());
        assertThat(returnedResponseDto.getUserTestStageProgressId(), is(startTestResponseDto.getUserTestStageProgressId()));
    }

    @Test
    public void getNextQuestion(){
        TestStage testStage = new TestStage();
        UserTestProgress userTestProgress = generateUserTestProgress(testStage, 0);

        Question nextQuestion = new Question();
        nextQuestion.setId(1);
        testStage.setQuestions(List.of(new Question(), nextQuestion));

        QuestionDto questionDto = new QuestionDto();
        questionDto.setId(nextQuestion.getId());

        when(questionMapper.toDto(testService.getNextQuestion(userTestProgress.getId()))).thenReturn(questionDto);

        QuestionDto returnedQuestion = stagesController.getNextQuestion(userTestProgress.getId());
        assertThat(returnedQuestion, is(questionDto));
    }

    @Test
    public void answerTheQuestionTest(){
        int correctAnswerId = 101;
        UserTestProgress userTestProgress = generateSingleQuestionTestProgress(correctAnswerId);

        AnswerDto answerDto = new AnswerDto();
        answerDto.setId(101);
        answerDto.setAnswerText("answer");

        when(testService.answerTheQuestion(answerDto.getId(),userTestProgress.getId())).thenReturn(answerDto.getId());

        AnswerDto returnedAnswer = stagesController.answerTheQuestion(answerDto,userTestProgress.getId());
        assertThat(returnedAnswer.getId(),is(answerDto.getId()));
    }

    @Test
    public void getTestResultsTest(){
        TestStage testStage = new TestStage();
        testStage.setQuestions(List.of(new Question()));

        UserTestProgress userTestProgress = generateUserTestProgress(testStage, 0);
        userTestProgressRepository.save(userTestProgress);

        TestResultsDto testResultsDto = new TestResultsDto();
        testResultsDto.setCurrentPoints(userTestProgress.getPoints());
        testResultsDto.setMaxPoints(testStage.getQuestions().size());

        when(testService.getTestResults(testStage.getId())).thenReturn(userTestProgress);
        when(testResultsMapper.toDto(userTestProgress)).thenReturn(testResultsDto);

        TestResultsDto returnedTestResult = stagesController.getTestResults(testStage.getId());
        assertThat(returnedTestResult, is(testResultsDto));


    }

    private UserTestProgress generateUserTestProgress(TestStage testStage, int currentQuestionIndex) {
        UserTestProgress userTestProgress = new UserTestProgress();
        userTestProgress.setId(100);
        userTestProgress.setStatus(UserTestProgress.Status.FINISHED);
        userTestProgress.setCurrentQuestionIndex(currentQuestionIndex);
        userTestProgress.setStage(testStage);
        userTestProgress.setPoints(0);

        return userTestProgress;
    }

    private UserTestProgress generateSingleQuestionTestProgress(int correctAnswerId) {
        Question question = new Question();
        Answer answer = new Answer();
        answer.setId(correctAnswerId);
        question.setCorrectAnswer(answer);
        question.setPoints(1);

        TestStage testStage = new TestStage();
        testStage.setQuestions(List.of(question));
        return generateUserTestProgress(testStage, 0);
    }

}
