package ru.nsu.smart.edutests.dto.mappers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import ru.nsu.smart.edutests.dto.ProfileDto;
import ru.nsu.smart.edutests.model.Level;
import ru.nsu.smart.edutests.model.User;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ProfileMapperTest {
    private ProfileMapper profileDtoMapper = new ProfileMapper(new ModelMapper());

    private final String DEFAULT_LEVEL_NAME = "NAME";

    private final String DEFAULT_USER_NAME = "Jame";

    private final String DEFAULT_USER_EMAIL = "Jame@Jame.Jame";

    private final String DEFAULT_USER_FULL_NAME = "Jame Jame";

    private final int DEFAULT_TOTAL_POINTS = 777;

    private final int DEFAULT_LEVEL_NUMBER = 7;

    @Test
    @DisplayName("Маппинг User в ProfileDto")
    void toDtoTest() {
        Level level = new Level();
        level.setName(DEFAULT_LEVEL_NAME);
        level.setId(DEFAULT_LEVEL_NUMBER);

        User user = new User();
        user.setName(DEFAULT_USER_NAME);
        user.setLevel(level);
        user.setEmail(DEFAULT_USER_EMAIL);
        user.setFullName(DEFAULT_USER_FULL_NAME);
        user.setTotalPoints(DEFAULT_TOTAL_POINTS);

        ProfileDto profileDto = profileDtoMapper.toDto(user);

        assertThat(profileDto.getEmail(), is(user.getEmail()));
        assertThat(profileDto.getFullName(), is(user.getFullName()));
        assertThat(profileDto.getName(), is(user.getName()));
        assertThat(profileDto.getTotalPoints(), is(user.getTotalPoints()));
        assertThat(profileDto.getLevelNumber(), is(user.getLevel().getId()));
        assertThat(profileDto.getLevelName(), is(user.getLevel().getName()));
    }
}
