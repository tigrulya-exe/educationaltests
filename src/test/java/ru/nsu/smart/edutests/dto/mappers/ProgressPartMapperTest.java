package ru.nsu.smart.edutests.dto.mappers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.nsu.smart.edutests.dto.models.ProgressPartDto;
import ru.nsu.smart.edutests.model.Stage;
import ru.nsu.smart.edutests.model.Unit;
import ru.nsu.smart.edutests.util.MapperUtils;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ProgressPartMapperTest {

    private ProgressPartMapper progressPartMapper;

    private final String PROGRESS_PART_NAME = "NAME";

    private final int PROGRESS_PART_ID = 1;

    @BeforeEach
    void initMapper() {
        progressPartMapper = new ProgressPartMapper();
    }

    @Test
    @DisplayName("Маппинг ProgressPart в ProgressPartDto")
    void toDtoTest() {
        Stage stage = new Stage();
        stage.setId(PROGRESS_PART_ID);

        Unit progressPart = new Unit();
        progressPart.setName(PROGRESS_PART_NAME);
        progressPart.setId(PROGRESS_PART_ID);
        progressPart.setStages(List.of(new Stage()));

        ProgressPartDto progressPartDto = progressPartMapper.toDto(progressPart);

        assertThat(progressPartDto.getId(), is(PROGRESS_PART_ID));
        assertThat(progressPartDto.getName(), is(PROGRESS_PART_NAME));
        assertThat(progressPartDto.getChildren().size(), is(1));
    }

    @Test
    @DisplayName("Маппинг списка entity в список dto")
    void mapperUtilsTest() {
        Stage firstStage = new Stage();
        firstStage.setId(PROGRESS_PART_ID);
        Stage secondStage = new Stage();
        secondStage.setId(PROGRESS_PART_ID + 1);

        var resultList = MapperUtils.entitiesToDtos(List.of(firstStage, secondStage), progressPartMapper);
        assertThat(resultList.get(0).getId(), is(PROGRESS_PART_ID));
        assertThat(resultList.get(1).getId(), is(PROGRESS_PART_ID + 1));
    }
}
