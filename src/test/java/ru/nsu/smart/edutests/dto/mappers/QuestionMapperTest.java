package ru.nsu.smart.edutests.dto.mappers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import ru.nsu.smart.edutests.dto.models.AnswerDto;
import ru.nsu.smart.edutests.dto.models.QuestionDto;
import ru.nsu.smart.edutests.model.Answer;
import ru.nsu.smart.edutests.model.Question;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

public class QuestionMapperTest {

    @Mock
    private ModelMapper modelMapper;
    @InjectMocks
    private QuestionMapper questionMapper;

    private Question question;
    private Answer correctAnswer;
    private Answer wrongAnswer;

    @BeforeEach
    void initAllMocks() {
        initMocks(this);
        question = new Question();
        correctAnswer = new Answer();
        wrongAnswer = new Answer();
        question.setCorrectAnswer(correctAnswer);
        Stream<Answer> wrongAnswersStream = Stream.of(wrongAnswer);
        question.setWrongAnswers(wrongAnswersStream.collect(Collectors.toSet()));
    }

    @Test
    @DisplayName("Question -> QuestionDto")
    void toDtoTest() {
        QuestionDto questionDto = new QuestionDto();
        AnswerDto correctAnswerDto = new AnswerDto();
        AnswerDto wrongAnswerDto = new AnswerDto();
        Mockito.when(modelMapper.map(question, QuestionDto.class)).thenReturn(questionDto);
        Mockito.when(modelMapper.map(wrongAnswer, AnswerDto.class)).thenReturn(wrongAnswerDto);
        Mockito.when(modelMapper.map(correctAnswer, AnswerDto.class)).thenReturn(correctAnswerDto);

        Stream<AnswerDto> answerDtoStream = Stream.of(wrongAnswerDto, correctAnswerDto);
        questionDto.setAnswerDtos(answerDtoStream.collect(Collectors.toSet()));

        QuestionDto result = questionMapper.toDto(question);
        assertThat(result, is(questionDto));
        QuestionDto nullResult = questionMapper.toDto(null);
        Assertions.assertNull(nullResult);
    }
}
