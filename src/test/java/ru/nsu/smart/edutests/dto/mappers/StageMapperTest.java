package ru.nsu.smart.edutests.dto.mappers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import ru.nsu.smart.edutests.dto.models.StageDto;
import ru.nsu.smart.edutests.model.Stage;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

public class StageMapperTest {

    private static  final String STAGE_TEST_NAME = "NAME";
    private static final int STAGE_TEST_ID = 1;
    private static final String STAGE_TEST_TEXT = "TEXT";

    private StageMapper stageMapper = new StageMapper ();

    @Test
    @DisplayName("Stage -> StageDto")
    void toDtoTest() {

        Stage stage = new Stage();
        stage.setId(STAGE_TEST_ID);
        stage.setText(STAGE_TEST_TEXT);
        stage.setName(STAGE_TEST_NAME);
        StageDto stageDto = new StageDto();

        stageDto.setText(stage.getText());
        stageDto.setId(stage.getId());
        stageDto.setName(stage.getName());

        StageDto result = stageMapper.toDto(stage);
        assertThat(stageDto.getText(), is(result.getText()));
        assertThat(stageDto.getId(), is(result.getId()));
        assertThat(stageDto.getName(), is(result.getName()));
    }

}
