package ru.nsu.smart.edutests.dto.mappers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.nsu.smart.edutests.dto.models.TestResultsDto;
import ru.nsu.smart.edutests.model.TestStage;
import ru.nsu.smart.edutests.model.UserTestProgress;

public class TestResultsMapperTest {

    private static final int TEST_POINTS = 1;

    private TestResultsMapper testResultsMapper;

    @BeforeEach
    void init() {
        testResultsMapper = new TestResultsMapper();
    }

    @Test
    @DisplayName("UserTestProgress -> TestResultsDto")
    void toDtoTest() {
        UserTestProgress userTestProgress = new UserTestProgress();
        userTestProgress.setPoints(TEST_POINTS);
        TestStage testStage = new TestStage();
        testStage.setMaxPoints(TEST_POINTS);
        userTestProgress.setStage(testStage);

        TestResultsDto result = testResultsMapper.toDto(userTestProgress);
        Assertions.assertEquals(TEST_POINTS, result.getCurrentPoints());
        Assertions.assertEquals(TEST_POINTS, result.getMaxPoints());
    }
}
