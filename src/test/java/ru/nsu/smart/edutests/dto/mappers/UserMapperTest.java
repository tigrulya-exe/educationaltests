package ru.nsu.smart.edutests.dto.mappers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import ru.nsu.smart.edutests.model.User;
import ru.nsu.smart.edutests.security.model.UserDto;

import static org.mockito.MockitoAnnotations.initMocks;

public class UserMapperTest {

    @Mock
    private ModelMapper modelMapper;
    @InjectMocks
    private UserMapper userMapper;

    private UserDto userDto;
    private User user;

    @BeforeEach
    void init() {
        initMocks(this);
        user = new User();
        userDto = new UserDto();
    }

    @Test
    @DisplayName("UserDto -> User")
    void toEntityTest() {
        Mockito.when(modelMapper.map(userDto, User.class)).thenReturn(user);
        User result = userMapper.toEntity(userDto);
        Assertions.assertEquals(result, user);
        Mockito.verify(modelMapper).map(userDto, User.class);
    }

    @Test
    @DisplayName("User -> UserDto")
    void toDto() {
        Mockito.when(modelMapper.map(user, UserDto.class)).thenReturn(userDto);
        UserDto result = userMapper.toDto(user);
        Assertions.assertEquals(result, userDto);
        Mockito.verify(modelMapper).map(user, UserDto.class);
    }
}
