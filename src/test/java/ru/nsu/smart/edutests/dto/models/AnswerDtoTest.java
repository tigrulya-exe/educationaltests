package ru.nsu.smart.edutests.dto.models;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class AnswerDtoTest {

    private static final int ANSWER_DTO_TEST_ID = 1;

    private static final String ANSWER_DTO_TEST_TEXT = "TEXT";

    @Test
    @DisplayName("Создание AnswerDto")
    void constructorTest() {
        AnswerDto answerDto = new AnswerDto();
        answerDto.setId(ANSWER_DTO_TEST_ID);
        answerDto.setAnswerText(ANSWER_DTO_TEST_TEXT);

        assertThat(ANSWER_DTO_TEST_ID, is(answerDto.getId()));
        assertThat(ANSWER_DTO_TEST_TEXT, is(answerDto.getAnswerText()));
    }
}
