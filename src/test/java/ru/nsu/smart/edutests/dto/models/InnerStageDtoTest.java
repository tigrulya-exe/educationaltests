package ru.nsu.smart.edutests.dto.models;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class InnerStageDtoTest {

    private static final String INNER_STAGE_DTO_TEST_NAME = "NAME";
    private static final int INNER_STAGE_DTO_TEST_ID = 1;
    private static final boolean INNER_STAGE_DTO_TEST_IS_TEST = true;

    @Test
    @DisplayName("Создание InnerStageDto")
    void constructorTest() {
        InnerStageDto innerStageDto = new InnerStageDto(INNER_STAGE_DTO_TEST_ID,
                INNER_STAGE_DTO_TEST_NAME,INNER_STAGE_DTO_TEST_IS_TEST);
        innerStageDto.setId(INNER_STAGE_DTO_TEST_ID);
        innerStageDto.setName(INNER_STAGE_DTO_TEST_NAME);

        assertThat(INNER_STAGE_DTO_TEST_ID, is(innerStageDto.getId()));
        assertThat(INNER_STAGE_DTO_TEST_NAME, is(innerStageDto.getName()));
    }
}
