package ru.nsu.smart.edutests.dto.models;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Set;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class QuestionDtoTest {

    private static final int QUESTION_DTO_TEST_ID = 1;

    private static final String QUESTION_DTO_TEST_TEXT = "TEXT";


    @Test
    @DisplayName("Создание QuestionDto")
    void ConstructorTest() {
        AnswerDto answerDto = new AnswerDto();
        QuestionDto questionDto =new QuestionDto();
        questionDto.setId(QUESTION_DTO_TEST_ID);
        questionDto.setQuestionText(QUESTION_DTO_TEST_TEXT);
        questionDto.setAnswerDtos(Set.of(answerDto));

        assertThat(QUESTION_DTO_TEST_ID, is(questionDto.getId()));
        assertThat(QUESTION_DTO_TEST_TEXT, is(questionDto.getQuestionText()));
        assertThat(Set.of(answerDto), is(questionDto.getAnswerDtos()));
    }
}
