package ru.nsu.smart.edutests.email;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

public class LocalEmailServiceTest {
    private static final String[] TEST_TO = { "to" };
    private static final String TEST_SUBJECT = "subject";
    private static final String TEST_TEXT ="sample_text";


    @Mock
    private JavaMailSender emailSender;

    @InjectMocks
    private LocalEmailService localEmailService;

    @BeforeEach
    void initAllMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @DisplayName("Тест для sendMessage с массивом")
    public void sendMessageTestArray() {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(TEST_TO);
        message.setSubject(TEST_SUBJECT);
        message.setText(TEST_TEXT);
        localEmailService.sendMessage(TEST_TO, TEST_SUBJECT, TEST_TEXT);
        Mockito.verify(emailSender, Mockito.times(1)).send(Mockito.eq(message));
    }
}
