package ru.nsu.smart.edutests.repositories;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.nsu.smart.edutests.model.UserRole;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@ImportAutoConfiguration(exclude = {FlywayAutoConfiguration.class})
public class RolesRepositoryIntegrationTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRolesRepository userRolesRepository;


    @Test
    void findUserRoles() {
        UserRole userRole = new UserRole();
        userRole.setRole(UserRole.Role.UNCONFIRMED);
        entityManager.persist(userRole);
        entityManager.flush();

        UserRole returnedUserRole = userRolesRepository.findByRole(UserRole.Role.UNCONFIRMED);
        assertThat(returnedUserRole, notNullValue());
    }
}
