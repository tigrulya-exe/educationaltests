package ru.nsu.smart.edutests.security;

import io.jsonwebtoken.JwtException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.MockitoAnnotations.initMocks;

public class ExceptionFilterTest {
    @Mock
    private FilterChain filterChain;

    private ExceptionHandlerFilter exceptionHandlerFilter;

    @BeforeEach
    void initAllMocks() {
        initMocks(this);
        exceptionHandlerFilter = new ExceptionHandlerFilter();
    }

    @Test
    @DisplayName("Проверка обработки исключений, связанных с проверкой jwt")
    void testJwtExceptionFiltering() throws IOException, ServletException {
        HttpServletResponse httpServletResponse = new MockHttpServletResponse();

        Mockito.doThrow(new JwtException("Error")).when(filterChain).doFilter(any(), any());
        exceptionHandlerFilter.doFilterInternal(null, httpServletResponse, filterChain);
        assertThat(httpServletResponse.getStatus(), is(HttpServletResponse.SC_UNAUTHORIZED));
    }

    @Test
    @DisplayName("Проверка обработки исключений, не связанных с проверкой jwt")
    void testOtherExceptionsFiltering() throws IOException, ServletException {
        HttpServletResponse httpServletResponse = new MockHttpServletResponse();

        Mockito.doThrow(new RuntimeException("Error")).when(filterChain).doFilter(any(), any());
        exceptionHandlerFilter.doFilterInternal(null, httpServletResponse, filterChain);
        assertThat(httpServletResponse.getStatus(), is(HttpServletResponse.SC_INTERNAL_SERVER_ERROR));
    }
}
