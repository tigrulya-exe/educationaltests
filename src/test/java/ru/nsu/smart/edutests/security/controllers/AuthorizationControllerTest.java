package ru.nsu.smart.edutests.security.controllers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.web.servlet.ModelAndView;
import ru.nsu.smart.edutests.dto.mappers.UserMapper;
import ru.nsu.smart.edutests.model.User;
import ru.nsu.smart.edutests.security.model.Credentials;
import ru.nsu.smart.edutests.security.model.NewPasswordDto;
import ru.nsu.smart.edutests.security.model.TokenDto;
import ru.nsu.smart.edutests.security.model.UserDto;
import ru.nsu.smart.edutests.security.services.DatabaseAuthorizationService;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

public class AuthorizationControllerTest {

    private static final String TEST_TOKEN_STRING = "TEST_TOKEN_STRING";
    private static final String TEST_EMAIL = "test@test.test";
    private static final String TEST_PASSWORD = "T3stPa55w0rd";
    private static final String TEST_VIEW_NAME_1 = "/restore.html";
    private static final String TEST_VIEW_NAME_2 = "/emailConfirmSuccess.html";

    @Mock
    private DatabaseAuthorizationService authorizationService;
    @Mock
    private UserMapper userMapper;
    @InjectMocks
    private AuthorizationController authorizationController;

    private User user;

    @BeforeEach
    void initAllMocks() {
        initMocks(this);
        user = new User();
    }

    @Test
    @DisplayName("Тест для метода signIn")
    void signInTest() {
        TokenDto tokenDto = new TokenDto(TEST_TOKEN_STRING);
        Mockito.when(authorizationService.authenticate(Mockito.any())).thenReturn(tokenDto);
        TokenDto result = authorizationController.signIn(new Credentials());
        assertThat(result, is(tokenDto));
        Mockito.verify(authorizationService).authenticate(Mockito.any());
    }

    @Test
    @DisplayName("Тест для метода signUp")
    void signUpTest() {
        UserDto userDto = new UserDto();
        Mockito.when(userMapper.toEntity(Mockito.any())).thenReturn(user);
        authorizationController.signUp(userDto);
        Mockito.verify(authorizationService).signUp(Mockito.any());
    }

    @Test
    @DisplayName("Тест для метода restore")
    void restoreTest() {
        authorizationController.restore(TEST_EMAIL);
        Mockito.verify(authorizationService).restorePassword(Mockito.any());

        NewPasswordDto newPasswordDto = new NewPasswordDto();
        newPasswordDto.setNewPassword(TEST_PASSWORD);
        authorizationController.restore(newPasswordDto, TEST_TOKEN_STRING);
        Mockito.verify(authorizationService).changePassword(Mockito.any(), Mockito.any());
    }

    @Test
    @DisplayName("Тест для метода getRestorePage")
    void getRestorePageTest() {
        ModelAndView result = authorizationController.getRestorePage(TEST_TOKEN_STRING);
        Mockito.verify(authorizationService).validateRestoreToken(Mockito.any());
        Assertions.assertEquals(TEST_VIEW_NAME_1, result.getViewName());
    }

    @Test
    @DisplayName("Тест для метода confirmRegistration")
    void confirmRegistrationTest() {
        ModelAndView result = authorizationController.confirmRegistration(TEST_TOKEN_STRING);
        Mockito.verify(authorizationService).confirmEmail(Mockito.any());
        Assertions.assertEquals(TEST_VIEW_NAME_2, result.getViewName());
    }
}
