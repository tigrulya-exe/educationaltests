package ru.nsu.smart.edutests.security.jwt;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.Authentication;
import ru.nsu.smart.edutests.model.User;
import ru.nsu.smart.edutests.services.UsersService;

public class JwtAuthenticationProviderTest {
    private static final String TEST_JWT = "TEST";
    private static final String TEST_JWT_USER_ID = "0";

    @Mock
    private UsersService usersService;

    @Mock
    private JwtProvider jwtProvider;

    @Mock
    private JwtAuthentication jwtAuthentication;

    @Mock
    private User user;


    @InjectMocks
    private JwtAuthenticationProvider jwtAuthenticationProvider;


    @BeforeEach
    void initAllMocks() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(jwtAuthentication.getJwt()).thenReturn(TEST_JWT);
        Mockito.when(jwtProvider.getUserId(TEST_JWT)).thenReturn(TEST_JWT_USER_ID);
        Mockito.when(usersService.getById(Integer.parseInt(TEST_JWT_USER_ID))).thenReturn(user);
    }

    @Test
    @DisplayName("Проверка неправильной авторизации")
    public void unsuccessfulAuthenticationTest() {
        Mockito.when(jwtProvider.validateToken(TEST_JWT)).thenReturn(false);
        Authentication returned = jwtAuthenticationProvider.authenticate(jwtAuthentication);
        Mockito.verify(jwtAuthentication, Mockito.times(0)).setUser(Mockito.any());
        Mockito.verify(jwtAuthentication, Mockito.times(0)).setAuthenticated(Mockito.anyBoolean());
        Assertions.assertNull(returned);
    }

    @Test
    @DisplayName("Проверка правильной авторизации")
    public void successfulAuthenticationTest() {
        Mockito.when(jwtProvider.validateToken(TEST_JWT)).thenReturn(true);
        Authentication returned = jwtAuthenticationProvider.authenticate(jwtAuthentication);
        Mockito.verify(jwtAuthentication, Mockito.times(1)).setUser(user);
        Mockito.verify(jwtAuthentication, Mockito.times(1)).setAuthenticated(true);
        Assertions.assertEquals(returned, jwtAuthentication);
    }
}
