package ru.nsu.smart.edutests.security.jwt;

import io.jsonwebtoken.MalformedJwtException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import javax.servlet.http.HttpServletRequest;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.util.UUID;

import static org.mockito.Mockito.*;

public class JwtProviderTest {

    @Mock
    private HttpServletRequest request;

    private static final String AUTH_HEADER = "Authorization";

    private static final String BEARER_PREFIX = "Bearer ";

    private static final String KEY = "passw0rd";

    private static final int USER_ID = 7771;

    private static final int EXPIRATION_MS = 1728000;

    private JwtProvider jwtTokenProvider;

    private Clock clock;

    @BeforeEach
    void setUp() {
        clock = Clock.fixed(Instant.parse("2020-03-09T10:00:00Z"), ZoneId.of("UTC"));
        jwtTokenProvider = new JwtProvider(KEY, Duration.ofMillis(EXPIRATION_MS), clock);
    }

    @Test
    @DisplayName("Получение имени пользователя из токена")
    void getUser() {
        String jwt = jwtTokenProvider.generateToken(USER_ID);

        String returnedUsername = jwtTokenProvider.getUserId(jwt);
        Assertions.assertEquals(String.valueOf(USER_ID), returnedUsername);
    }

    @Test
    @DisplayName("Валидация истекшего токена")
    void validateExpiredToken() {
        jwtTokenProvider = spy(new JwtProvider(KEY, Duration.ofMillis(-1), clock));
        String jwt = jwtTokenProvider.generateToken(USER_ID);

        Assertions.assertFalse(jwtTokenProvider.validateToken(jwt));
        verify(jwtTokenProvider, times(1)).isTokenExpired(jwt);
    }

    @Test
    @DisplayName("Валидация некорректного токена (случайная строка)")
    void validateWrongToken() {
        Assertions.assertThrows(MalformedJwtException.class, () -> jwtTokenProvider.validateToken(UUID.randomUUID().toString()));
    }

    @Test
    @DisplayName("Тест для getTokenFromRequest not null")
    void getTokenFromRequestTest() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(request.getHeader(AUTH_HEADER)).thenReturn(BEARER_PREFIX);
        Assertions.assertEquals("", jwtTokenProvider.getTokenFromRequest(request));
    }

    @Test
    @DisplayName("Тест для getTokenFromRequest null")
    void getTokenFromRequestNull() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(request.getHeader(AUTH_HEADER)).thenReturn(null);
        Assertions.assertNull(jwtTokenProvider.getTokenFromRequest(request));
    }
}
