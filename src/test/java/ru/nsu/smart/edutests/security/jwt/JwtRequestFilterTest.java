package ru.nsu.smart.edutests.security.jwt;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtRequestFilterTest {

    private static final String TEST_STRING_JWT = "TEST";

    @Mock
    private JwtProvider jwtProvider;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private FilterChain filterChain;

    @InjectMocks
    private JwtRequestFilter jwtRequestFilter;

    @BeforeEach
    void initAllMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @DisplayName("Тест фильтра (строка null)")
    public void doFilterInternalTest() throws ServletException, IOException {
        jwtRequestFilter.doFilterInternal(request, response, filterChain);
        Mockito.verify(filterChain, Mockito.times(1)).doFilter(request, response);
    }

    @Test
    @DisplayName("Тест фильтра (строка не null)")
    public void doFilterInternalTestNull() throws ServletException, IOException {
        Mockito.when(jwtProvider.getTokenFromRequest(request)).thenReturn(TEST_STRING_JWT);
        jwtRequestFilter.doFilterInternal(request, response, filterChain);
        JwtAuthentication jwtAuthentication = (JwtAuthentication) SecurityContextHolder.getContext().getAuthentication();
        Assertions.assertNotNull(jwtAuthentication);
        Assertions.assertEquals(TEST_STRING_JWT, jwtAuthentication.getJwt());
        Mockito.verify(filterChain, Mockito.times(1)).doFilter(request,response);
    }
}
