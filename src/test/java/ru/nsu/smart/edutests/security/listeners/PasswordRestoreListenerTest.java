package ru.nsu.smart.edutests.security.listeners;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import ru.nsu.smart.edutests.ApplicationProperties;
import ru.nsu.smart.edutests.email.EmailService;
import ru.nsu.smart.edutests.model.User;
import ru.nsu.smart.edutests.security.events.PasswordRestoreEvent;
import ru.nsu.smart.edutests.security.model.Token;
import ru.nsu.smart.edutests.security.services.DatabaseTokenService;

public class PasswordRestoreListenerTest {

    private static final String TEST_DOMAIN_NAME = "test.test";
    private static final Integer TEST_PORT = 9999;
    private static final String TEST_TOKEN_STRING_REPRESENTATION = "test_token_representation";
    private static final String TEST_USER_NAME = "TEST";
    private static final String TEST_USER_EMAIL = "test@test.test";
    private static final String TEST_EMAIL_SUBJECT = "Смена пароля";
    private static final String TEST_EMAIL_MESSAGE = "Здравствуйте, " + TEST_USER_NAME
            + "!\nНажмите на ссылку ниже, чтобы поменять пароль:\n" + "http://"
            + TEST_DOMAIN_NAME + ":" + TEST_PORT + "/restore/" + TEST_TOKEN_STRING_REPRESENTATION;

    @Mock
    private ApplicationProperties applicationProperties;

    @Mock
    private DatabaseTokenService tokenService;

    @Mock
    private EmailService emailService;

    @Mock
    private PasswordRestoreEvent event;

    private Token token;

    private User user;

    @InjectMocks
    private PasswordRestoreListener passwordRestoreListener;

    @BeforeEach
    void initAllMocks() {
        MockitoAnnotations.initMocks(this);
        user = new User();
        user.setName(TEST_USER_NAME);
        user.setEmail(TEST_USER_EMAIL);
        token = new Token();
        token.setStringRepresentation(TEST_TOKEN_STRING_REPRESENTATION);
    }

    @Test
    @DisplayName("Отправить на почту ссылку для замены пароля")
    public void onApplicationEventTest() {
        Mockito.when(event.getUser()).thenReturn(user);
        Mockito.when(applicationProperties.getDomainName()).thenReturn(TEST_DOMAIN_NAME);
        Mockito.when(applicationProperties.getPort()).thenReturn(TEST_PORT);
        Mockito.when(tokenService.generateToken(Mockito.any(), Mockito.any())).thenReturn(token);

        passwordRestoreListener.onApplicationEvent(event);

        Mockito.verify(emailService).sendMessage(TEST_USER_EMAIL, TEST_EMAIL_SUBJECT, TEST_EMAIL_MESSAGE);
    }
}
