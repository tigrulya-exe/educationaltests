package ru.nsu.smart.edutests.security.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.nsu.smart.edutests.model.Level;
import ru.nsu.smart.edutests.model.User;
import ru.nsu.smart.edutests.model.UserRole;
import ru.nsu.smart.edutests.repositories.LevelsRepository;
import ru.nsu.smart.edutests.repositories.UserRolesRepository;
import ru.nsu.smart.edutests.repositories.UsersRepository;
import ru.nsu.smart.edutests.security.AuthorizationTokenProvider;
import ru.nsu.smart.edutests.security.events.PasswordRestoreEvent;
import ru.nsu.smart.edutests.security.model.Credentials;
import ru.nsu.smart.edutests.security.model.Token;
import ru.nsu.smart.edutests.security.model.TokenDto;
import ru.nsu.smart.edutests.services.UsersService;

import java.util.Optional;

public class AuthorizationServiceTest {

    private static final String TEST_USER_EMAIL = "test@test.test";
    private static final String TEST_USER_PASSWORD = "TEST";
    private static final String TEST_USER_PASSWORD_ENCODED = "ENCODED_TEST";
    private static final String TEST_USER_NAME = "TEST_NAME";
    private static final String TEST_TOKEN_STRING = "TEST_STRING_REPRESENTATION";
    private static final int TEST_USER_ID = 1;

    @Mock
    private UsersService usersService;

    @Mock
    private AuthorizationTokenProvider jwtProvider;

    @Mock
    private PasswordEncoder bCryptPasswordEncoder;

    @Mock
    private ApplicationEventPublisher eventPublisher;

    @Mock
    private UserRolesRepository rolesRepository;

    @Mock
    private LevelsRepository levelsRepository;

    @Mock
    private UsersRepository usersRepository;

    @Mock
    private DatabaseTokenService tokenService;

    @Mock
    private UserRole testRole;

    private User user;

    private Level level;

    private Optional<User> optionalUser;

    private Optional<Level> optionalLevel;


    @InjectMocks
    private DatabaseAuthorizationService authorizationService;


    @BeforeEach
    void initAllMocks() {
        MockitoAnnotations.initMocks(this);
        user = new User();
        user.setPassword(TEST_USER_PASSWORD);
        level = new Level();
        user.setId(TEST_USER_ID);
        user.setName(TEST_USER_NAME);
        optionalUser = Optional.of(user);
        optionalLevel = Optional.of(level);
        Mockito.when(levelsRepository.findById(1)).thenReturn(optionalLevel);
        Mockito.when(usersRepository.findByEmailIgnoreCase(Mockito.anyString())).thenReturn(optionalUser);
        Mockito.when(rolesRepository.findByRole(UserRole.Role.UNCONFIRMED)).thenReturn(testRole);
        Mockito.when(bCryptPasswordEncoder.encode(TEST_USER_PASSWORD)).thenReturn(TEST_USER_PASSWORD_ENCODED);
    }

    @Test
    @DisplayName("Тест для signUp")
    public void signUpTest() {
        Mockito.when(usersRepository.save(user)).thenReturn(user); // probably should've used spy
        authorizationService.signUp(user);
        //Mockito.verify(authorizationService, Mockito.times(1)).checkUniqueParams(user);
        //no private method testing amirite?
        Assertions.assertEquals(TEST_USER_PASSWORD_ENCODED, user.getPassword());
        Assertions.assertTrue(user.getRoles().contains(testRole));
        Assertions.assertEquals(user.getLevel(), level);
    }

    @Test
    @DisplayName("Тест для authenticate")
    public void authenticateTest() {
        Mockito.when(bCryptPasswordEncoder.matches(TEST_USER_PASSWORD, TEST_USER_PASSWORD_ENCODED)).thenReturn(true);
        User signedUpUser = new User();
        signedUpUser.setName(TEST_USER_NAME);
        signedUpUser.setPassword(TEST_USER_PASSWORD_ENCODED);
        Credentials credentials = new Credentials();
        credentials.setLogin(TEST_USER_NAME);
        credentials.setPassword(TEST_USER_PASSWORD);
        Mockito.when(usersService.getUserByNickname(credentials.getLogin())).thenReturn(signedUpUser);
        Mockito.when(jwtProvider.generateToken(Mockito.anyInt())).thenReturn(TEST_TOKEN_STRING);
        TokenDto resToken = authorizationService.authenticate(credentials);
        TokenDto testToken = new TokenDto(TEST_TOKEN_STRING);
        Assertions.assertEquals(testToken, resToken);
    }

    @Test
    @DisplayName("Тест для restorePassword")
    public void restorePasswordTest() {
        Mockito.when(usersService.getUserByEmail(TEST_USER_EMAIL)).thenReturn(user);
        authorizationService.restorePassword(TEST_USER_EMAIL);
        Mockito.verify(eventPublisher, Mockito.times(1))
                .publishEvent(Mockito.refEq(new PasswordRestoreEvent(user), "timestamp"));
    }

    @Test
    @DisplayName("Тест для valitdateRestoreToken")
    public void validateRestoreTokenTest() {
        String token = TEST_TOKEN_STRING;
        authorizationService.validateRestoreToken(token);
        Mockito.verify(tokenService, Mockito.times(1))
                .validateToken(token, Token.Type.PASSWORD_RESTORE);
    }

    @Test
    @DisplayName("Тест для changePassword")
    public void changePasswordTest() {
        Token token = new Token();
        User user = new User();
        Mockito.when(tokenService.validateToken(TEST_TOKEN_STRING, Token.Type.PASSWORD_RESTORE))
                .thenReturn(token);
        Mockito.when(tokenService.getUser(TEST_TOKEN_STRING, Token.Type.PASSWORD_RESTORE))
                .thenReturn(user);
        authorizationService.changePassword(TEST_TOKEN_STRING, TEST_USER_PASSWORD);
        Assertions.assertEquals(user.getPassword(), bCryptPasswordEncoder.encode(TEST_USER_PASSWORD));
        Mockito.verify(tokenService, Mockito.times(1)).removeToken(token.getId());
    }

    @Test
    @DisplayName("Тест для confirmEmail")
    public void confirmEmailTest() {
        Token token = new Token();
        User user = new User();
        Mockito.when(tokenService.validateToken(TEST_TOKEN_STRING, Token.Type.EMAIL_CONFIRM))
                .thenReturn(token);
        Mockito.when(tokenService.getUser(TEST_TOKEN_STRING, Token.Type.EMAIL_CONFIRM))
                .thenReturn(user);
        UserRole unconfirmedUserRole = new UserRole();
        UserRole defaultUserRole = new UserRole();
        unconfirmedUserRole.setRole(UserRole.Role.UNCONFIRMED);
        defaultUserRole.setRole(UserRole.Role.DEFAULT);
        user.getRoles().add(unconfirmedUserRole);
        authorizationService.confirmEmail(TEST_TOKEN_STRING);
        Mockito.verify(tokenService, Mockito.times(1)).removeToken(token.getId());
    }
}
