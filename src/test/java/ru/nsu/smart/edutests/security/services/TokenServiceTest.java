package ru.nsu.smart.edutests.security.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import ru.nsu.smart.edutests.model.User;
import ru.nsu.smart.edutests.repositories.TokenRepository;
import ru.nsu.smart.edutests.security.model.Token;

import java.time.Clock;
import java.time.Duration;
import java.util.Date;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.MockitoAnnotations.initMocks;

public class TokenServiceTest {

    private static final String TEST_TOKEN_STRING_REPRESENTATION = "test_token_representation";
    private static final String TEST_EXCEPTION_MESSAGE_WRONG_TOKEN = "Wrong token";
    private static final int TEST_TOKEN_ID = 1;

    @Mock
    private TokenRepository repository;
    @Mock
    private Clock clock;
    @InjectMocks
    private DatabaseTokenService tokenService;

    private User user;
    private Token token;

    private Clock workingClock;
    private Date oldDate;
    private Date futureDate;
    private long nowDateLong;

    @BeforeEach
    void initAllMocks() {
        initMocks(this);
        user = new User();
        token = new Token();
        token.setStringRepresentation(TEST_TOKEN_STRING_REPRESENTATION);
        token.setType(Token.Type.REFRESH);
        token.setUser(user);

        workingClock = Clock.systemDefaultZone();
        nowDateLong = workingClock.millis();
        oldDate = new Date(nowDateLong - 1000);
        futureDate = new Date(nowDateLong + 1000);
    }

    @Test
    @DisplayName("Получение токена по идентификатору и типу")
    void getTokenByStringAndTypeTest() {
        Mockito.when(repository.findByStringRepresentationAndType(Mockito.any(), Mockito.any())).thenReturn(Optional.of(token));
        Token result = tokenService.getTokenByStringAndType(TEST_TOKEN_STRING_REPRESENTATION, Token.Type.REFRESH);
        assertThat(result, is(token));
        Mockito.verify(repository).findByStringRepresentationAndType(Mockito.any(), Mockito.any());
    }

    @Test
    @DisplayName("Получение пользователя по идентификатору и типу токена")
    void getUserTest() {
        Mockito.when(repository.findByStringRepresentationAndType(Mockito.any(), Mockito.any())).thenReturn(Optional.of(token));
        User result = tokenService.getUser(TEST_TOKEN_STRING_REPRESENTATION, Token.Type.REFRESH);
        assertThat(result, is(user));
    }

    @Test
    @DisplayName("Проверка на истечение срока действия токена")
    void isTokenExpiredTest() {
        Token t = new Token();
        Mockito.when(clock.millis()).thenReturn(nowDateLong);
        t.setExpirationDate(oldDate);
        assertThat(tokenService.isTokenExpired(t), is(true));
        t.setExpirationDate(futureDate);
        assertThat(tokenService.isTokenExpired(t), is(false));
    }

    @Test
    @DisplayName("Валидация токена по идентификатору и типу")
    void validateTokenTest() {
        Token t = new Token();
        Mockito.when(repository.findByStringRepresentationAndType(Mockito.any(), Mockito.any())).thenReturn(Optional.of(t));
        Mockito.when(clock.millis()).thenReturn(nowDateLong);
        t.setExpirationDate(futureDate);
        Token result1 = tokenService.validateToken(TEST_TOKEN_STRING_REPRESENTATION, Token.Type.REFRESH);
        assertThat(result1, is(t));
        t.setExpirationDate(oldDate);
        try {
            Token result2 = tokenService.validateToken(TEST_TOKEN_STRING_REPRESENTATION, Token.Type.REFRESH);
        } catch (IllegalArgumentException e) {
            assertThat(e.getLocalizedMessage(), is(TEST_EXCEPTION_MESSAGE_WRONG_TOKEN));
        }
    }

    @Test
    @DisplayName("Генерация токена")
    void generateTokenTest() {
        Duration testDuration = Duration.ofDays(1);
        DatabaseTokenService tokenService1 = new DatabaseTokenService(repository, workingClock, testDuration);
        Mockito.when(repository.save(Mockito.any())).then(returnsFirstArg());
        Token result = tokenService1.generateToken(user, Token.Type.REFRESH);
        assertThat(result.getUser(), is(user));
        assertThat(result.getType(), is(Token.Type.REFRESH));
        Mockito.verify(repository).save(Mockito.any());
    }

    @Test
    @DisplayName("Удаление токена")
    void removeTokenTest() {
        tokenService.removeToken(TEST_TOKEN_ID);
        Mockito.verify(repository).deleteById(Mockito.any());
    }
}
