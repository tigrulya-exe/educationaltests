package ru.nsu.smart.edutests.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.smart.edutests.model.ProgressPart;
import ru.nsu.smart.edutests.model.User;

import java.util.Optional;
import java.util.function.Supplier;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.MockitoAnnotations.initMocks;

public abstract class ProgressPartServiceTest<E extends ProgressPart> {
    private static final int DEFAULT_PROGRESS_PART_ID = 1;

    private static final int DEFAULT_PROGRESS_PART_REQUIRED_POINTS = 0;

    private static final int DEFAULT_PROGRESS_PART_SERIAL_NUMBER = 1;

    private static final String DEFAULT_PROGRESS_PART_NAME = "TEST PROGRESS NAME";

    @Mock
    private UsersService usersService;

    @Mock
    private JpaRepository<E, Integer> progressPartRepository;

    private Supplier<E> progressPartSupplier;

    private User user;

    @InjectMocks
    private ProgressPartService<E> progressPartService;

    public ProgressPartServiceTest(Supplier<E> progressPartSupplier) {
        this.progressPartSupplier = progressPartSupplier;
    }

    @BeforeEach
    void initAllMocks() {
        initMocks(this);
        user = new User();
        user.setId(0);
        user.setTotalPoints(0);
        user.setName("TEST");
    }

    @Test
    @DisplayName("Получение этапа прогресса под номером id")
    void getEntity() {
        E progressPart = generateProgressPart();

        Mockito.when(usersService.getCurrentUser()).thenReturn(user);
        Mockito.when(progressPartRepository.findById(DEFAULT_PROGRESS_PART_ID)).thenReturn(Optional.of(progressPart));
        Mockito.when(progressPartRepository.save(any())).then(returnsFirstArg());

        ProgressPart result = progressPartService.getEntity(DEFAULT_PROGRESS_PART_ID);
        assertThat(result.getRequiredPoints(), is(DEFAULT_PROGRESS_PART_REQUIRED_POINTS));
        assertThat(result.getName(), is(DEFAULT_PROGRESS_PART_NAME));
        assertThat(result.getSerialNumber(), is(DEFAULT_PROGRESS_PART_SERIAL_NUMBER));
    }


    private E generateProgressPart() {
        E progressPart = progressPartSupplier.get();
        progressPart.setRequiredPoints(DEFAULT_PROGRESS_PART_REQUIRED_POINTS);
        progressPart.setName(DEFAULT_PROGRESS_PART_NAME);
        progressPart.setSerialNumber(DEFAULT_PROGRESS_PART_SERIAL_NUMBER);
        return progressPart;
    }

}
