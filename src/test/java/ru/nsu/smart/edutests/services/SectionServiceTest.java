package ru.nsu.smart.edutests.services;

import ru.nsu.smart.edutests.model.Section;

public class SectionServiceTest extends ProgressPartServiceTest<Section> {
    public SectionServiceTest() {
        super(Section::new);
    }
}
