package ru.nsu.smart.edutests.services;

import ru.nsu.smart.edutests.model.Stage;

public class StageServiceTest extends ProgressPartServiceTest<Stage> {
    public StageServiceTest() {
        super(Stage::new);
    }
}
