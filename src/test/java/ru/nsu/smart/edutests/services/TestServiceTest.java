package ru.nsu.smart.edutests.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import ru.nsu.smart.edutests.model.*;
import ru.nsu.smart.edutests.repositories.TestsRepository;
import ru.nsu.smart.edutests.repositories.UserTestProgressRepository;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.MockitoAnnotations.initMocks;


public class TestServiceTest {

    private static final int DEFAULT_TEST_PROGRESS_ID = 777;

    private static final int DEFAULT_TEST_STAGE_ID = 908;

    private static final int DEFAULT_ANSWER_ID = 730;

    private static final int DEFAULT_QUESTION_ID = 30;

    private static final int DEFAULT_POINTS_PER_QUESTION = 2;

    @Mock
    private UsersService usersService;

    @Mock
    private TestsRepository testsRepository;

    @Mock
    private UserTestProgressRepository userTestProgressRepository;

    @InjectMocks
    private TestService testService;

    private User user;

    @BeforeEach
    void initAllMocks() {
        initMocks(this);
        user = new User();
        user.setId(0);
        user.setTotalPoints(0);
        user.setName("TEST");
    }

    @Test
    @DisplayName("Открытие сессии тестирования")
    void startTest() {
        TestStage testStage = new TestStage();

        Mockito.when(usersService.getCurrentUser()).thenReturn(user);
        Mockito.when(testsRepository.findById(DEFAULT_TEST_STAGE_ID)).thenReturn(Optional.of(testStage));
        Mockito.when(userTestProgressRepository.save(any())).then(returnsFirstArg());
        UserTestProgress returned = testService.startTest(DEFAULT_TEST_STAGE_ID);

        assertThat(returned.getPoints(), is(0));
        assertThat(returned.getCurrentQuestionIndex(), is(0));
        assertThat(returned.getStatus(), is(UserTestProgress.Status.ANSWERED));
        assertThat(returned.getStage(), is(testStage));
        assertThat(returned.getUser(), is(user));
    }

    @Test
    @DisplayName("Получить следующий вопрос в тесте")
    void getNextQuestion() {
        TestStage testStage = new TestStage();
        UserTestProgress userTestProgress = generateUserTestProgress(testStage, 1);

        Question nextQuestion = new Question();
        nextQuestion.setId(1);
        testStage.setQuestions(List.of(new Question(), nextQuestion));

        Mockito.when(userTestProgressRepository.findById(DEFAULT_TEST_PROGRESS_ID)).thenReturn(Optional.of(userTestProgress));
        Question returnedQuestion = testService.getNextQuestion(userTestProgress.getId());

        assertThat(returnedQuestion, is(nextQuestion));
        assertThat(userTestProgress.getCurrentQuestionIndex(), is(1));
        assertThat(userTestProgress.getStatus(), is(UserTestProgress.Status.IN_PROGRESS));
    }

    @Test
    @DisplayName("Получить следующий вопрос в заверщенном тесте")
    void getNextQuestionInFinishedTest() {
        Question question = new Question();
        TestStage testStage = new TestStage();
        testStage.setQuestions(List.of(question));
        testStage.setId(DEFAULT_TEST_STAGE_ID);

        UserTestProgress userTestProgress = generateUserTestProgress(testStage, 0);
        userTestProgress.setStatus(UserTestProgress.Status.IN_PROGRESS);

        Mockito.when(userTestProgressRepository.findById(DEFAULT_TEST_PROGRESS_ID)).thenReturn(Optional.of(userTestProgress));
        Question returnedQuestion = testService.getNextQuestion(userTestProgress.getId());
        assertThat(returnedQuestion, is(question));
    }

    @Test
    @DisplayName("Закончить тест, если вопросов не осталось")
    void ifNoQuestionsFinishTest() {
        TestStage testStage = new TestStage();
        testStage.setQuestions(List.of(new Question()));

        UserTestProgress userTestProgress = generateUserTestProgress(testStage, 1);
        userTestProgress.setUser(user);

        Mockito.when(userTestProgressRepository.findById(DEFAULT_TEST_PROGRESS_ID)).thenReturn(Optional.of(userTestProgress));
        Question returnedQuestion = testService.getNextQuestion(userTestProgress.getId());

        assertThat(returnedQuestion, is(nullValue()));
        assertThat(userTestProgress.getCurrentQuestionIndex(), is(1));
        assertThat(userTestProgress.getStatus(), is(UserTestProgress.Status.FINISHED));
    }

    @Test
    @DisplayName("Получить результаты законченного теста")
    void getFinishResults() {
        UserTestProgress userTestProgress = generateSingleQuestionTestProgress(DEFAULT_QUESTION_ID);
        userTestProgress.setStatus(UserTestProgress.Status.FINISHED);

        Mockito.when(userTestProgressRepository.findById(DEFAULT_TEST_PROGRESS_ID)).thenReturn(Optional.of(userTestProgress));
        assertThat(testService.getTestResults(DEFAULT_TEST_PROGRESS_ID), is(userTestProgress));
    }

    @Test
    @DisplayName("Получить результаты незаконченного теста")
    void getNotFinishedResults() {
        UserTestProgress userTestProgress = generateSingleQuestionTestProgress(DEFAULT_QUESTION_ID);

        Mockito.when(userTestProgressRepository.findById(DEFAULT_TEST_PROGRESS_ID)).thenReturn(Optional.of(userTestProgress));
        assertThat(exceptionOf(() -> testService.getTestResults(DEFAULT_TEST_PROGRESS_ID)),
                instanceOf(IllegalArgumentException.class));
    }

    @Test
    @DisplayName("Ответить на вопрос законченного теста")
    void answerFinishedTestQuestion() {
        UserTestProgress userTestProgress = generateSingleQuestionTestProgress(DEFAULT_QUESTION_ID);
        userTestProgress.setCurrentQuestionIndex(1);

        Mockito.when(userTestProgressRepository.findById(DEFAULT_TEST_PROGRESS_ID)).thenReturn(Optional.of(userTestProgress));
        assertThat(exceptionOf(() -> testService.answerTheQuestion(DEFAULT_ANSWER_ID, DEFAULT_TEST_PROGRESS_ID)),
                instanceOf(IllegalArgumentException.class));
    }


    @Test
    @DisplayName("Правильный ответить на вопрос")
    void answerTheQuestionCorrectly() {
        int correctAnswerId = DEFAULT_ANSWER_ID;

        UserTestProgress userTestProgress = generateSingleQuestionTestProgress(correctAnswerId);
        answerTheQuestion(userTestProgress, correctAnswerId);

        assertThat(userTestProgress.getPoints(), is(DEFAULT_POINTS_PER_QUESTION));
    }

    @Test
    @DisplayName("Непавильный ответить на вопрос")
    void answerTheQuestionWrong() {
        int correctAnswerId = DEFAULT_ANSWER_ID - 1;

        UserTestProgress userTestProgress = generateSingleQuestionTestProgress(correctAnswerId);
        answerTheQuestion(userTestProgress, correctAnswerId);

        assertThat(userTestProgress.getPoints(), is(0));
    }

    private void answerTheQuestion(UserTestProgress userTestProgress, int correctAnswerId) {
        Mockito.when(userTestProgressRepository.findById(DEFAULT_TEST_PROGRESS_ID)).thenReturn(Optional.of(userTestProgress));
        int returnedCorrectAnswerId = testService.answerTheQuestion(DEFAULT_ANSWER_ID, DEFAULT_TEST_PROGRESS_ID);

        assertThat(userTestProgress.getStatus(), is(UserTestProgress.Status.ANSWERED));
        assertThat(returnedCorrectAnswerId, is(correctAnswerId));
    }

    private UserTestProgress generateSingleQuestionTestProgress(int correctAnswerId) {
        Question question = new Question();
        Answer answer = new Answer();
        answer.setId(correctAnswerId);
        question.setCorrectAnswer(answer);
        question.setPoints(DEFAULT_POINTS_PER_QUESTION);

        TestStage testStage = new TestStage();
        testStage.setQuestions(List.of(question));
        testStage.setId(DEFAULT_TEST_STAGE_ID);
        return generateUserTestProgress(testStage, 0);
    }

    private static Throwable exceptionOf(Callable<?> callable) {
        try {
            callable.call();
            return null;
        } catch (Throwable t) {
            return t;
        }
    }

    private UserTestProgress generateUserTestProgress(TestStage testStage, int currentQuestionIndex) {
        UserTestProgress userTestProgress = new UserTestProgress();
        userTestProgress.setId(DEFAULT_TEST_PROGRESS_ID);
        userTestProgress.setUser(user);
        userTestProgress.setStatus(UserTestProgress.Status.ANSWERED);
        userTestProgress.setCurrentQuestionIndex(currentQuestionIndex);
        userTestProgress.setStage(testStage);
        userTestProgress.setPoints(0);

        return userTestProgress;
    }
}
