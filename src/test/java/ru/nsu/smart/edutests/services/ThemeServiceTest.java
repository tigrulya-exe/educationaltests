package ru.nsu.smart.edutests.services;

import ru.nsu.smart.edutests.model.Theme;

public class ThemeServiceTest extends ProgressPartServiceTest<Theme> {

    public ThemeServiceTest() {
        super(Theme::new);
    }
}
