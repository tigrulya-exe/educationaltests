package ru.nsu.smart.edutests.services;

import ru.nsu.smart.edutests.model.Unit;

public class UnitServiceTest extends ProgressPartServiceTest<Unit> {
    public UnitServiceTest() {
        super(Unit::new);
    }
}
