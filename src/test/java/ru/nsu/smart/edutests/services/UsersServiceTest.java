package ru.nsu.smart.edutests.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import ru.nsu.smart.edutests.exceptions.NotFoundException;
import ru.nsu.smart.edutests.model.User;
import ru.nsu.smart.edutests.repositories.UsersRepository;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

public class UsersServiceTest {

    private static final String TEST_USER_NAME = "USER";
    private static final String TEST_USER_EMAIL = "test@test.test";
    private static final int TEST_USER_ID = 1;
    private static final String TEST_BAD_USER_NAME = "BAD";
    private static final String TEST_BAD_USER_EMAIL = "bad@bad.bad";
    private static final int TEST_BAD_USER_ID = 2;
    private static final String TEST_EXC_MSG_VALIDATION_FAIL = "Wrong test progress id";
    private static final String TEST_EXC_MSG_WRONG_NICKNAME = "Wrong nickname";
    private static final String TEST_EXC_MSG_WRONG_EMAIL = "Wrong email";
    private static final String TEST_EXC_MSG_UNAUTHORIZED = "Unauthorized";
    private static final String TEST_EXC_MSG_USER_NOT_FOUND = "User not found";

    @Mock
    private UsersRepository userRepository;
    @InjectMocks
    private UsersService usersService;

    private User user;
    private User badUser;

    @BeforeEach
    void initAllMocks() {
        initMocks(this);
        user = new User();
        badUser = new User();
        Mockito.when(userRepository.findCurrentUser()).thenReturn(Optional.of(user));
        Mockito.when(userRepository.findByNameIgnoreCase(TEST_USER_NAME)).thenReturn(Optional.of(user));
        Mockito.when(userRepository.findByEmailIgnoreCase(TEST_USER_EMAIL)).thenReturn(Optional.of(user));
        Mockito.when(userRepository.findById(TEST_USER_ID)).thenReturn(Optional.of(user));
    }

    @Test
    @DisplayName("Получение юзера по его никнейму")
    void getUserByNicknameTest() {
        User result = usersService.getUserByNickname(TEST_USER_NAME);
        assertThat(result, is(user));
        Mockito.verify(userRepository).findByNameIgnoreCase(TEST_USER_NAME);

        try {
            usersService.getUserByNickname(TEST_BAD_USER_NAME);
        } catch (IllegalArgumentException e) {
            assertThat(e.getLocalizedMessage(), is(TEST_EXC_MSG_WRONG_NICKNAME));
        }
    }

    @Test
    @DisplayName("Получение юзера по его почте")
    void getUserByEmailTest() {
        User result = usersService.getUserByEmail(TEST_USER_EMAIL);
        assertThat(result, is(user));
        Mockito.verify(userRepository).findByEmailIgnoreCase(TEST_USER_EMAIL);

        try {
            usersService.getUserByEmail(TEST_BAD_USER_EMAIL);
        } catch (IllegalArgumentException e) {
            assertThat(e.getLocalizedMessage(), is(TEST_EXC_MSG_WRONG_EMAIL));
        }
    }

    @Test
    @DisplayName("Получение текущего юзера")
    void getCurrentUserTest() {
        User result = usersService.getCurrentUser();
        assertThat(result, is(user));
        Mockito.verify(userRepository).findCurrentUser();

        Mockito.when(userRepository.findCurrentUser()).thenReturn(Optional.of(badUser));
        try {
            usersService.getCurrentUser();
        } catch (IllegalArgumentException e) {
            assertThat(e.getLocalizedMessage(), is(TEST_EXC_MSG_UNAUTHORIZED));
        }
    }

    @Test
    @DisplayName("Валидация юзера")
    void validateUserTest() {
        usersService.validateUser(user);
        Mockito.verify(userRepository).findCurrentUser();

        try {
            usersService.validateUser(badUser);
        } catch (IllegalArgumentException e) {
            assertThat(e.getLocalizedMessage(), is(TEST_EXC_MSG_VALIDATION_FAIL));
        }
    }

    @Test
    @DisplayName("Получение юзера по его id")
    void getByIdTest() {
        User result = usersService.getById(TEST_USER_ID);
        assertThat(result, is(user));
        Mockito.verify(userRepository).findById(TEST_USER_ID);

        try {
            usersService.getById(TEST_BAD_USER_ID);
        } catch (NotFoundException e) {
            assertThat(e.getLocalizedMessage(), is(TEST_EXC_MSG_USER_NOT_FOUND));
        }
    }
}
